<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Di_Designs
 */

get_header(); ?>

<?php $insBg = get_field('background_image_insp', 'option'); ?> 
<div class="row insp-corner" style="background-image: url(<?php echo $insBg; ?>);">

	<div class="inside">
		
		<div class="marg">
		
			<div class="sixty extra-padd">
			
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="Di Designs" class="logo-insp">
				
				<?php the_field('content', 'option'); ?>
				
			</div>
			
		</div>
		
	</div>
	
	<img src="<?php the_field('image_insp', 'option'); ?>" class="abs-image-insp">
	
</div>

<div class="crumbs row">
	
		<div class="inside">
			<?php wordpress_breadcrumbs(); ?>
		</div>
		
</div>

<div class="row page-content">

	<div class="inside">
	
		<div class="row filter-collections">
			
				<a href="/inspiration/" class="letter alt <?php if(!is_category()) { echo 'active'; } ?>">
						All
				</a>
			
				<?php $currentCat = get_queried_object_id(); ?>
			
				<?php $cats = get_categories('exclude=1&title_li=&hide_empty=0'); foreach($cats as $cat) { ?>
			
					<a href="<?php echo get_term_link($cat->term_id); ?>" class="letter alt <?php if($cat->term_id === $currentCat) { echo 'active'; } ?>">
						<?php echo $cat->name; ?>
					</a>
			
				<?php } ?>	
			
		</div>
		
		<div class="row">
		
			<?php if(!is_category()) { ?>
 			<?php $published_posts = wp_count_posts()->publish; $totalPosts = $published_posts -1; ?>
			<?php } else { 
			$category = get_term($currentCat, 'category');
			$published_posts = $category->count;
			} ?>
			
			<div class="marg no-flex">
			
				<div class="row newsers">
				
				<?php $num = 0; while ( have_posts() ) : the_post(); ?>
				
					<?php if($num === 0 || $num === 10 || $num === 20 || $num === 30 || $num === 40 || $num === 50 || $num === 60 || $num === 70 || $num === 80 || $num === 90 || $num === 100 || $num === 110 || $num === 120 || $num === 130) { ?>
					<div class="row flex-this">
					
					<div class="fourty">
						
					<?php } ?>
						
					<?php if($num === 5 || $num === 15 || $num === 25 || $num === 35  || $num === 45 || $num === 55 || $num === 65 || $num === 75 || $num === 85 || $num === 95 || $num === 105  || $num === 115 || $num === 125 || $num === 135) { ?>
					<div class="row flex-this">
					
					<div class="sixty">
						
					<?php } ?>
						
					<?php if($num === 3 || $num === 8 || $num === 13 || $num === 18 || $num === 23 || $num === 28 || $num === 33 || $num === 38 || $num === 43 || $num === 48 || $num === 53 || $num === 58 || $num === 63 || $num === 68 || $num === 73 || $num === 78 || $num === 83 || $num === 88 || $num === 93 || $num === 98 || $num === 103 || $num === 108 || $num === 113 || $num === 118 || $num === 123 || $num === 128) { ?>
					<div class="row flex-this smaller">
					<?php } ?>
										
					<?php if($num == 0 || $num == 7 || $num == 10 || $num === 17 || $num === 20 || $num === 27  || $num === 30 || $num === 37  || $num === 40  || $num === 47  || $num === 50  || $num === 57  || $num === 60  || $num === 67  || $num === 70  || $num === 77  || $num === 80  || $num === 87  || $num === 90  || $num === 97  || $num === 100  || $num === 107  || $num === 110  || $num === 117  || $num === 120  || $num === 127  || $num === 130) { ?>
					
						<div class="news-item item-<?php echo $num; ?>">
				
							<div class="img-item-news">
							<a href="<?php the_permalink(); ?>">
							
								<span class="row img-wrap">
									<?php the_post_thumbnail('rect'); ?>
								</span>
								
							</a>
							</div>
								
								<span class="cta-box-info row">
									
									<div class="row wrap-cats-loop">
										<?php the_category(', '); ?>
									</div>

									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

									<div class="row blurb">
										<?php echo wp_trim_words(get_the_excerpt(), 20); ?>
									</div>

									<div class="row button-wrap">
										<a href="<?php the_permalink(); ?>" class="green">Read more</a>
									</div>

								</span>
								
							</a>
							
						</div>
				
					<?php } ?>
					
					<?php if($num === 3 || $num === 8 || $num === 13 || $num === 18 || $num === 23 || $num === 28 || $num === 33 || $num === 38 || $num === 43 || $num === 48 || $num === 53 || $num === 58 || $num === 63 || $num === 68 || $num === 73 || $num === 78 || $num === 83 || $num === 88 || $num === 93 || $num === 98 || $num === 103 || $num === 108 || $num === 113 || $num === 118 || $num === 123 || $num === 128) { ?>
	
					<div class="news-item widest item-<?php echo $num; ?>">
				
						<div class="img-item-news">
						
							<a href="<?php the_permalink(); ?>">
							
								<span class="row img-wrap">
									<?php the_post_thumbnail('blog-square'); ?>
								</span>
								
							</a>
							
						</div>
								
							<div class="wrap-cta">
						
								<span class="cta-box-info row">
									
									<div class="row wrap-cats-loop">
										<?php the_category(', '); ?>
									</div>

									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

									<div class="row blurb">
										<?php echo wp_trim_words(get_the_excerpt(), 20); ?>
									</div>

									<div class="row button-wrap">
										<a href="<?php the_permalink(); ?>" class="green">Read more</a>
									</div>

								</span>
								
							</div>
							
						</div>
						
					<?php } ?>
						
					<?php if($num === 4 || $num === 9 || $num === 14 || $num === 19 || $num === 24 || $num === 29 || $num === 34 || $num === 39 || $num === 44 || $num === 49 || $num === 54 || $num === 59 || $num === 64 || $num === 69 || $num === 74 || $num === 79 || $num === 84 || $num === 89 || $num === 94 || $num === 99 || $num === 104 || $num === 109 || $num === 114 || $num === 119 || $num === 124) { ?>
						
					<div class="news-item smallest item-<?php echo $num; ?>">
				
							<div class="wrap-cta">
						
								<span class="cta-box-info row">
									
									<div class="row wrap-cats-loop">
										<?php the_category(', '); ?>
									</div>

									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

									<div class="row blurb">
										<?php echo wp_trim_words(get_the_excerpt(), 20); ?>
									</div>

									<div class="row button-wrap">
										<a href="<?php the_permalink(); ?>" class="green">Read more</a>
									</div>

								</span>
								
							</div>
							
						</div>	
						
					<?php } ?>
						
					<?php if($num === 1 || $num === 2 || $num === 5 || $num === 6 || $num === 11 || $num === 12 || $num === 15 || $num === 16 || $num === 21 || $num === 22 || $num === 25 || $num === 26 || $num === 31  || $num === 32 || $num === 35  || $num === 36  || $num === 41  || $num === 42  || $num === 45  || $num === 46  || $num === 51  || $num === 52  || $num === 55  || $num === 56  || $num === 61  || $num === 62  || $num === 65  || $num === 66  || $num === 71  || $num === 72  || $num === 75  || $num === 76  || $num === 81  || $num === 82  || $num === 85  || $num === 86  || $num === 91  || $num === 92  || $num === 95  || $num === 96  || $num === 101  || $num === 102  || $num === 105  || $num === 106  || $num === 111  || $num === 112  || $num === 115  || $num === 116  || $num === 121  || $num === 122  || $num === 125  || $num === 126  || $num === 131) { ?>
			
					<div class="news-item wider <?php if($num % 2 == 0){ echo 'flip'; } ?> item-<?php echo $num; ?>">
				
						<div class="img-item-news">
						
							<a href="<?php the_permalink(); ?>">
							
								<span class="row img-wrap">
									<?php the_post_thumbnail('blog-square'); ?>
								</span>
								
							</a>
							
						</div>
								
							<div class="wrap-cta">
						
								<span class="cta-box-info row">
									
									<div class="row wrap-cats-loop">
										<?php the_category(', '); ?>
									</div>

									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

									<div class="row blurb">
										<?php echo wp_trim_words(get_the_excerpt(), 20); ?>
									</div>

									<div class="row button-wrap">
										<a href="<?php the_permalink(); ?>" class="green">Read more</a>
									</div>

								</span>
								
							</div>
							
						</div>
			
					<?php } ?>
				
					<?php if($num === 0 || $num === 10 || $num === 20 || $num === 30 || $num === 40 || $num === 50 || $num === 60 || $num === 70 || $num === 80 || $num === 90 || $num === 100 || $num === 110 || $num === 120 || $num === 130) { ?>
						</div>
						<div class="sixty">
					<?php } ?>	
							
					<?php if($num === 2 || $num === 12 || $num === 22 || $num === 32 || $num === 42 || $num === 52 || $num === 62 || $num === 72 || $num === 82 || $num === 92 || $num === 102 || $num === 112 || $num === 122 || $num === 132) { ?>
						</div>
					<?php } ?>	
						
					<?php if($totalPosts === $num) { ?>
					</div>
					<?php } ?>	
						
					<?php if($num === 6 || $num === 16  || $num === 26 || $num === 36 || $num === 46 || $num === 56 || $num === 66 || $num === 76 || $num === 86 || $num === 96 || $num === 106 || $num === 116 || $num === 126) { ?>
						</div>
						<div class="fourty">
					<?php } ?>	
							
					<?php if($num === 7 || $num === 17 || $num === 27 || $num === 37 || $num === 47 || $num === 57 || $num === 67 || $num === 77 || $num === 87 || $num === 97 || $num === 107 || $num === 117 || $num === 127 || $num === 137) { ?>
						</div>
					<?php } ?>	
						
					<?php if($totalPosts === $num) { ?>
					</div>
					<?php } ?>	
						
					<?php if($num === 2 || $num === 4 || $num === 7 || $num == 9 || $num === 12 || $num === 14 || $num === 17 || $num === 19 || $num === 22 || $num === 24 || $num === 27 || $num === 29 || $num === 32 || $num === 34 || $num === 37 || $num === 39 || $num === 42 || $num === 44 || $num === 47 || $num === 49 || $num === 52 || $num === 54 || $num === 57 || $num === 59 || $num === 62 || $num === 64 || $num === 67 || $num === 69 || $num === 72 || $num === 74 || $num === 77 || $num === 79 || $num === 82 || $num === 84 || $num === 87 || $num === 89 || $num === 92 || $num === 94 || $num === 97 || $num === 99 || $num === 102 || $num === 104 || $num === 107 || $num === 109 || $num === 112 || $num === 114 || $num === 117 || $num === 119 || $num === 122 || $num === 124 ) { ?>
				
						</div><!-- close flex -->
				
					<?php } ?>
				
				<?php $num++; endwhile; ?>
				
				</div>
								
			</div>
			
			<?php if($published_posts > 10) { ?>
			<div class="row center">
								
					<div class="page-load-status">
						<div class="loader-ellips infinite-scroll-request">
											
						</div>
					</div>
									
					<div class="view-more-button button lightgreen">
						Load More
					</div>
				
					<div class="row showing">
						Showing <span>10</span> of <?php echo $published_posts; ?> results
					</div>
									
				</div>
			<?php } ?>
		
			<div class="hidden-pager">
				
			<?php if( (!is_paged() || $paged === 0) && !is_category() ) { ?>
				
				<a class="page-numbers next" href="/inspiration/page/2">
				</a>
				
			<?php } elseif( (!is_paged() || $paged === 0) && is_category() ) { ?>
				
				<a class="page-numbers next" href="<?php echo get_category_link($currentCat); ?>/page/2">
				</a>
				
			<?php } ?>	
				
			<?php
			$big = 999999999;
 
        //your function for pagination or plugin you added for pagination
        
        echo paginate_links(array(
          'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
          'format' => '?paged=%#%',
          'current' => $paged,
					'prev_next' => true,
					'show_all' => true,
          ));
			
			?>
			</div>
		
		</div>
		
	</div>
	
</div>

<div class="wrap-blocks row">
<?php
	// Loop through rows.
$blockNum = 0; while ( have_rows('page_builder', 'option') ) : the_row();
  $fileToGet = get_row_layout().'.php';
	include(locate_template('partials/'.$fileToGet));
	
$blockNum++; endwhile; ?>
</div>

<?php get_footer(); ?>