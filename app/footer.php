<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Di_Designs
 * @since Di Designs 1.0
 */
?>

<?php if(is_shop()) { $pageID = get_option( 'woocommerce_shop_page_id' ); } elseif(is_home()) { $pageID = get_option('page_for_posts'); } else { $pageID = get_the_ID(); } ?>

<?php $insp = get_field('include_inspiration_corner_cta', $pageID); if($insp) { ?>

<?php include(locate_template('partials/inspiration_corner.php')); ?>

<?php } ?>

<?php if(is_tax('yith_product_brand')) { ?>

<?php include(locate_template('partials/inspiration_corner.php')); ?>

<?php } ?>
 
<?php $showCtas = get_field('include_footer_cta_boxes'); if($showCtas) { ?>
<div class="row footer-ctas">
	
	<div class="inside">
		
		<div class="marg">
		
			<div class="row wrap-ctas">
			
			<?php while(has_sub_field('cta_boxes', 'option')): ?>
	
				<div class="third">
			
					<a href="<?php the_sub_field('link'); ?>">
						
						<span class="img-wrap">
						
							<?php
							$image = get_sub_field('image');
							if( $image ):

									// Image variables.
									$url = $image['url'];
									$title = $image['title'];
									$alt = $image['alt'];
									$caption = $image['caption'];

									// Thumbnail size attributes.
									$size = 'rect';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];

									 ?>

									<img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" />

							<?php endif; ?>
							
						</span>
						
						<span class="cta-box-info row">
						
							<h3><?php the_sub_field('title'); ?></h3>
							
							<div class="row blurb">
								<?php the_sub_field('blurb'); ?>
							</div>
								
							<div class="row button-wrap">
								<span class="button lightgreen">Learn more</span>
							</div>
							
						</span>
						
					</a>
					
				</div>
			
			<?php endwhile; ?>
			
		</div>
			
		</div>
		
	</div>
	
</div>
<?php } ?>

<?php if(is_tax('yith_product_brand') || is_tax('product_cat')) { ?>
<div class="row footer-ctas">
	
	<div class="inside">
	
		<div class="marg">
		
			<div class="row wrap-ctas">
			
			<?php while(has_sub_field('cta_boxes', 'option')): ?>
	
				<div class="third">
			
					<a href="<?php the_sub_field('link'); ?>">
						
						<span class="img-wrap">
						
							<?php
							$image = get_sub_field('image');
							if( $image ):

									// Image variables.
									$url = $image['url'];
									$title = $image['title'];
									$alt = $image['alt'];
									$caption = $image['caption'];

									// Thumbnail size attributes.
									$size = 'rect';
									$thumb = $image['sizes'][ $size ];
									$width = $image['sizes'][ $size . '-width' ];
									$height = $image['sizes'][ $size . '-height' ];

									 ?>

									<img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" />

							<?php endif; ?>
							
						</span>
						
						<span class="cta-box-info row">
						
							<h3><?php the_sub_field('title'); ?></h3>
							
							<div class="row blurb">
								<?php the_sub_field('blurb'); ?>
							</div>
								
							<div class="row button-wrap">
								<span class="button lightgreen">Learn more</span>
							</div>
							
						</span>
						
					</a>
					
				</div>
			
			<?php endwhile; ?>
			
			</div>	
				
		</div>
		
	</div>
	
</div>
<?php } ?>
  
<?php $tradeBg = get_field('background_image', 'option'); ?>
<div class="row footer-trade" style="background-image: url(<?php echo $tradeBg; ?>);">

	<div class="wrapper-inner">
	
		<h2><?php the_field('title', 'option'); ?></h2>
		
		<?php 
		$link = get_field('button', 'option');
		if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="button lightgreen" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>
		
		<?php the_field('extra_content', 'option'); ?>
		
	</div>
	
</div>

<footer class="row">

	<div class="inside">
        
		<div class="footer-top row">
			
			<?php echo do_shortcode('[instagram-feed num=5 cols=5 showfollow=false]'); ?>
			
		</div>
		
		<div class="row footer-main">
		
			<div class="marg">
			
				<div class="fourty">
				
					<div class="footer-title">
						Contact us
					</div>
					
					<?php the_field('about_us_content', 'option'); ?>
					
					<div class="row c-links">
					
						<?php $tel = get_field('phone_number', 'option'); if($tel) { ?>
							<a href="tel<?php echo $tel; ?>">
								
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#435053;}</style></defs><path class="a" d="M13.954,18.746c-2.643,0-6.03-2.368-8.318-4.584C2.6,11.225.794,8.019.794,5.586.794,2.744,3.564.437,5.372.437a1.7,1.7,0,0,1,1.473.839L8.56,4.135a1.705,1.705,0,0,1-.4,2.214L6.445,7.726a17.045,17.045,0,0,0,5.369,5.37l1.376-1.717a1.707,1.707,0,0,1,2.213-.4L18.266,12.7a1.7,1.7,0,0,1,.838,1.472C19.1,15.976,16.8,18.746,13.954,18.746ZM5.372,1.581A3.543,3.543,0,0,0,3.206,2.76,4.179,4.179,0,0,0,1.938,5.586c0,2.1,1.722,5.069,4.494,7.754,2.673,2.589,5.626,4.262,7.522,4.262a4.183,4.183,0,0,0,2.816-1.26,3.538,3.538,0,0,0,1.189-2.173.567.567,0,0,0-.279-.49l-2.863-1.717a.562.562,0,0,0-.736.135L12.393,14.2a.573.573,0,0,1-.736.136A18.18,18.18,0,0,1,5.2,7.884a.574.574,0,0,1,.135-.737L7.445,5.458a.564.564,0,0,0,.133-.736L5.862,1.863A.566.566,0,0,0,5.372,1.581Z"/></svg>
								
								<span><?php echo $tel; ?></span>
								
							</a>
						<?php } ?>
						
						<?php $email = get_field('email_address', 'option'); if($email) { ?>
							<a href="mailto<?php echo $email; ?>">
								
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#435053;}</style></defs><path class="a" d="M16.323,2.686H3.574A2.127,2.127,0,0,0,1.45,4.811v9.561A2.127,2.127,0,0,0,3.574,16.5H16.323a2.127,2.127,0,0,0,2.125-2.125V4.811A2.127,2.127,0,0,0,16.323,2.686Zm1.063,11.686a1.064,1.064,0,0,1-1.063,1.063H3.574a1.064,1.064,0,0,1-1.062-1.063V4.811A1.064,1.064,0,0,1,3.574,3.748H16.323a1.064,1.064,0,0,1,1.063,1.063Z"/><path class="a" d="M16.185,4.984a.53.53,0,0,0-.75-.035L10.764,9.2l0,0-.1.089a1.058,1.058,0,0,1-1.43,0l-.1-.09h0L4.463,4.949a.531.531,0,0,0-.715.786L7.991,9.591,3.748,13.448a.53.53,0,0,0-.035.75.531.531,0,0,0,.75.036L8.8,10.289a2.11,2.11,0,0,0,2.294,0l4.339,3.945a.531.531,0,0,0,.715-.786L11.907,9.591,16.15,5.735A.531.531,0,0,0,16.185,4.984Z"/></svg>
								
								<span><?php echo $email; ?></span>
								
							</a>
						<?php } ?>
						
					</div>
					
					<div class="row social">
					
						<?php while(has_sub_field('social_media_networks','option')): ?>

						<?php $attachment_id_icon = get_sub_field('icon'); ?>					

						<?php $url = get_sub_field('url','option'); ?>

						<a target="_blank" href="<?php echo $url; ?>" title="<?php the_sub_field('name'); ?>"><i class="fa <?php echo $attachment_id_icon; ?>"></i></a>

						<?php endwhile;  ?>
						
					</div>
					
					<img src="<?php the_field('payment_types_icons', 'option'); ?>" alt="Payment types" class="ccicons">
					<br />
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/HSA logo.png" alt="Homestage" width="100">
					
				</div>
				
				<div class="sixty">
				
					<div class="marg">
						
						<div class="third">
					
							<div class="footer-title">
								Collections
							</div>

							<ul><?php wp_nav_menu(array('theme_location' => 'footer1', 'container' => '')); ?></ul>

						</div>
						
						<div class="third">
					
							<div class="footer-title">
								Product types
							</div>

							<ul><?php wp_nav_menu(array('theme_location' => 'footer2', 'container' => '')); ?></ul>

						</div>
						
						<div class="third">
					
							<div class="footer-title">
								Company
							</div>

							<ul><?php wp_nav_menu(array('theme_location' => 'footer3', 'container' => '')); ?></ul>

						</div>
						
					</div>
					
				</div>
				
			</div>
			
		</div>
		
  </div>
    
</footer>

<div class="row copy-bar">

	<div class="inside">
	
		<div class="copy-left">
			&copy; <?php echo date('Y'); ?> D.I. Designs. All rights reserved.
		</div>
		
		<div class="copy-right">
			<ul><?php wp_nav_menu(array('theme_location' => 'footercopy', 'container' => '')); ?></ul>
		</div>
		
	</div>
	
</div>

</div><!------------ wrap-site ---------------->

<?php wp_footer(); ?>

</body>
</html>