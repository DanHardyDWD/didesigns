jQuery(window).load(function() {
	
	if($('.flex-control-nav').length) {
		
		var normalthumbs = $('.flex-control-nav').bxSlider({ 
			pager:false,
			slideWidth:600,
			mode: 'vertical',
			minSlides: 3,
			maxSlides:3,
			moveSlides: 1,
			slideMargin:13,
			//infiniteLoop:true,
			hideControlOnEnd:true
		});
		
		var mobilethumbs =$('.mobile-thumbs').bxSlider({
			maxSlides: 3,
			minSlides: 3,
			'mode': 'horizontal',
			'pager': false,
			'moveSlides': 1,
			slideWidth: 180,
			adaptiveHeight:false,
			slideMargin:13,
		});
		
	}
	
	if($('.custom-image-slider').length) {
		var customSlider = $('.custom-image-slider').bxSlider({
			pager:true,
			controls:false,
			maxSlides:1,
			adaptiveheight:true,
			//onSlideAfter: function($slideElement, oldIndex, newIndex) {
        //normalthumbs.goToSlide(newIndex);
 			//}
		});

		$('.custom-open-slide-img').click(function() {

			var goTo = $(this).data('open');
			customSlider.goToSlide(goTo);
			$('.custom-open-slide-img').removeClass('active');
			$(this).addClass('active');

		});
	}
	
	$('select').niceSelect();
		
});
jQuery(document).ready(function($) {
	
	setTimeout(function() {
    var $tabs = jQuery( '.wc-tabs, ul.tabs' ).first();
    $tabs.parent().find('#tab-description').hide();
    $tabs.parent().find( '.description' ).removeClass('active');
	}, 10);
	
		$(document).on("click", ".tabs li", function() {
			if($(this).hasClass('open')) {
				$(this).next('div').hide();
				$(this).removeClass('open');
			} else {
				$(this).next('div').show();
				$('.tabs li').removeClass('open');
				$(this).addClass('open');
			}
		});
	
	var headHeight = $('header').outerHeight();
	$('.wrap-site').css('margin-top', headHeight);
	
	$(window).resize(function() {
		headHeight = $('header').outerHeight();
		$('.wrap-site').css('margin-top', headHeight);
	});
	
	if ($(window).scrollTop() > 60){  
		$('header, .menu-h').addClass("small");
	}
	else{
		$('header, .menu-h').removeClass("small");
	}
	
	$(window).scroll(function() {
		if ($(this).scrollTop() > 60){  
			$('header, .menu-h').addClass("small");
	  }
	  else{
			$('header, .menu-h').removeClass("small");
	  }
	});
	
	$('.stnd-content table').each(function() {
		$(this).wrap('<div class="row table-wrap"></div>');
	});
	
	if($('.table-wrap').length) {
		new SimpleBar($('.table-wrap')[0], { autoHide: false, forceVisible: false });
	}
	
	$('.make-v-slider').each(function() {
			
			 var vidSlider = $(this).bxSlider({
				minSlides:1,
				maxSlides:2,
				moveSlides:1,
				mode: 'horizontal',
				controls:false,
				adaptiveHeight:true,
				infiniteLoop: true,
				 pager:false
				 
		});
		
		$(this).parents('.collections-slider').find('.go-to-next').click(function() {
				vidSlider.goToNextSlide();
		});
		
		$(this).parents('.collections-slider').find('.go-to-prev').click(function() {
				vidSlider.goToPrevSlide(); 
		});
		
	});
	
	if($('.products .collection-item').length) {
	var t$iso = new InfiniteScroll('.products', {
 
      path : ".page-numbers.next",  
      append: '.collection-item',
      history: false,
			button: '.view-more-button',
		  scrollThreshold: false,
		  status: '.page-load-status',
                   // selector for all items you'll retrieve
  });
	
	t$iso.on( 'append', function( response, path, items ) {

			var visible = $(".collection-item:visible").length;
			$('.showing span').html(visible);

	});
		
	}
	
	if($('.products .download-cat').length) {
	var t$isoe = new InfiniteScroll('.products', {
 
      path : ".page-numbers.next",  
      append: '.download-cat',
      history: false,
			button: '.view-more-button',
		  scrollThreshold: false,
		  status: '.page-load-status',
                   // selector for all items you'll retrieve
  });
	
	t$isoe.on( 'append', function( response, path, items ) {

			var visible = $(".download-cat:visible").length;
			$('.showing span').html(visible);

	});
		
	}
	
	if($('.newsers').length) {
	var td$iso = new InfiniteScroll('.newsers', {
 
      path : ".page-numbers.next",  
      append: '.flex-this',
      history: false,
			button: '.view-more-button',
		  scrollThreshold: false,
		  status: '.page-load-status',
                   // selector for all items you'll retrieve
  });
		
	td$iso.on( 'append', function( response, path, items ) {

			var visible = $(".news-item:visible").length;
			$('.showing span').html(visible);

	});
		
	}
	
	$(document).on("click", ".yith-woo-ajax-navigation h3", function() {
		$(this).toggleClass('open');
		$(this).parent().find('ul').slideToggle();
	});
	
	$(".woo-sort").on('change', 'select', function() {
  $(this).parent().submit();
	});
	
	$('.letter').click(function() {
		var letter = $(this).children('span').html();
		$('#letter').val(letter);
		$(this).parents('form').submit();
	});
		
	$('.faq-title').click(function() {
		if($(this).hasClass('open')) {
			
			$(this).next('.faq-answer').slideUp();
			$(this).removeClass('open');
			
		} else {
		
			$('.faq-answer').slideUp();
			$(this).next('.faq-answer').slideDown();
			$(this).addClass('open');
			
		}
	
	});
	
	if($('.slider > .slide').length > 1) {
		$('.slider').bxSlider({
			controls:false,
			adaptiveHeight:true
		});
	}
	
	$('.wrap-ctas .third').matchHeight();
	$('.wrap-ctas .third a').matchHeight();
	
	$('.footer-title').click(function() {
		$(this).toggleClass('open');
		$(this).next('ul').toggleClass('open');
	});
	
	$('.mobile-open').click(function(e){
		e.preventDefault();
		$(this).toggleClass('open');
		$(".menu-h").slideToggle();
    $("body").toggleClass("active");
    $(".wrap-site").toggleClass("active");
	});
	
	$('.open-sub-sub').click(function() {
		$(this).toggleClass('open');
		$(this).parents('li').children('.sub-menu').toggleClass('open');
	});
	
	$('.down-to-col').click(function(col) {
		col.preventDefault();
		$('html, body').animate({ scrollTop: $('#col-area').offset().top -160 }, 400);
	});
	
	$('.view-full-desc.inner').click(function(desc) {
		desc.preventDefault();
		$('.description_tab').trigger('click');
		$('html, body').animate({ scrollTop: $('.tabs-area').offset().top -160 }, 400);
	});
	
	if($('.collection-gallery').length) {
		
		var settings = function() {
	
			
			var settings1 = {
				maxSlides: 4,
				minSlides: 4,
				'mode': 'horizontal',
				'pager': true,
				'controls': true,
				'moveSlides': 1,
				slideWidth: 640,
				adaptiveHeight:true,
				pager:false,
				infiniteLoop: true,
				hideControlOnEnd: true
				};
				var settings2 = {
				maxSlides: 3,
				minSlides: 3,
				'mode': 'horizontal',
				'controls': true,
				'moveSlides': 1,
				slideWidth: 640,
				adaptiveHeight:true,
				pager:false,
				infiniteLoop: true,
				hideControlOnEnd: true
				};
				var settings3 = {
				maxSlides: 2,
				minSlides: 2,
				'mode': 'horizontal',
				'controls': true,
				'moveSlides': 1,
				adaptiveHeight:true,
				slideWidth: 640,
				infiniteLoop: true,
				pager:false,
				hideControlOnEnd: true
				};
				var settings4 = {
				maxSlides: 1,
				minSlides: 1,
				slideWidth: 640,
				'mode': 'horizontal',
				infiniteLoop: true,
				'controls': true,
				'moveSlides': 1,
				adaptiveHeight:true,
				pager:false,
				hideControlOnEnd: true
				};
				if( ($(window).width()<=1130) && ($(window).width()>=840) ) {
					return settings2;	
				} else if( ($(window).width()<=840) && ($(window).width()>=580)  ) {
					return settings3;	
				} else if(($(window).width()<=580) ) {
					return settings4;	
				} else {
					return settings1;	
				}

				}

		var sliders = new Array();
	
		$('.collection-gallery').each(function(i, slider) {
		if($(this).find('.gallery-slide').length > 4) {
			
			sliders[i] = $(slider).bxSlider(settings());
									
		}
		
	});
		
	$(window).resize(function() {
			
		$.each(sliders, function(i, slider) { 
      slider.destroySlider();
			slider.bxSlider(settings());
    });
				
	});
		
	}
	
	$('.woo-side aside:first .widget-title').addClass('open');
	
	$('.image-zoom').each(function() {
    $(this).zoom({
      url: $(this).find('img.flex-active').attr('data-zoom')
   });
	});
	
});