<?php
/**
 * Template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Di_Designs
 * @since Di Designs 1.0
 */

get_header(); ?>

<div class="four-04 row page-content">

	<div class="inside">
	
		<div class="contain-width">
			
			<h1>Sorry, this page can't be found...</h1>
			
			<p>We couldn't find the page you were looking for. This is either because:</p>

			<p>There is an error in the URL entered into your web browser, please check the URL and try again</p>

			<p>The page you are looking for has been moved or deleted</p>
			
			<div class="row button-wrap">
				<a href="<?php home_url(); ?>" class="button lightgreen">Return home</a>
			</div>
				
		</div>
		
	</div>
    
</div>
<!-- #page-content -->

<?php get_footer(); ?>