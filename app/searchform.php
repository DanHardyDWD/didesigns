<?php
/**
 * Template for displaying search forms in Di Designs
 *
 * @package WordPress
 * @subpackage Di_Designs
 * @since Di Designs 1.0
 */
?>
	<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="text" class="field" name="s" id="s" placeholder="<?php esc_attr_e( 'What are you looking for?', 'didesigns' ); ?>" />
		<input type="submit" class="submit" name="submit" id="searchsubmit" value="" />
		<input type="hidden" name="post_type" value="product" />
	</form>
