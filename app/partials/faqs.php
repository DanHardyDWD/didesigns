<div class="row page-content faqs">
	
	<div class="inside">
	
		<?php while(has_sub_field('categories')): ?>
		
		<div class="row faq-cat">
		
			<h2><?php the_sub_field('title'); ?></h2>
			
			<?php while(has_sub_field('faqs')): ?>
			
			<div class="row faq">
			
				<div class="row faq-title">
					
					<?php the_sub_field('question'); ?>
					
					<span></span>
					
				</div>
				
				<div class="row faq-answer">
					<?php the_sub_field('answer'); ?>
				</div>
				
			</div>
			
			<?php endwhile; ?>
			
		</div>
		
		<?php endwhile; ?>
		
	</div>
	
	
</div>