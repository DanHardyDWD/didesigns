<div class="row feat-prod main-shop woocommerce">

	<div class="inside">
		
		<?php 
		$featprod = get_sub_field('product');
		$args = array ( 'post_type' => 'product', 'posts_per_page' => 1, 'post__in' => array($featprod) );
		query_posts( $args ); ?>

		<?php $product = get_product($featprod); while ( have_posts() ) : ?>
				<?php the_post(); ?>

		<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
		
				<div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images">
			
					<div class="custom-slider">
					
						<div class="row custom-image-slider">
						
							<div class="custom-image-slide woocommerce-product-gallery__image">
								<?php $post_thumbnail_id = $product->get_image_id(); ?>
								<a href="">
									<img src="<?php echo wp_get_attachment_image_url( $post_thumbnail_id, 'large'); ?>" alt="Two drawer desk" class="flex-active" draggable="false">
								</a>
							</div>

							<?php $attachment_ids = $product->get_gallery_image_ids(); ?>
							<?php foreach ( $attachment_ids as $attachment_id ) { ?>
							<div class="custom-image-slide woocommerce-product-gallery__image">
								<a href="">
									<img src="<?php echo wp_get_attachment_image_url( $attachment_id, 'large'); ?>" alt="Two drawer desk" class="flex-active" draggable="false">
								</a>
							</div>
							<?php } ?>
							
						</div>
						
					</div>
					
					<ol class="flex-control-nav flex-control-thumbs" style="width: auto; position: relative; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">

						<li class="custom-open-slide-img active" data-open="0">
							<img src="<?php echo wp_get_attachment_image_url( $post_thumbnail_id, 'thumbnail'); ?>" alt="<?php the_title(); ?>" class="flex-active" draggable="false">
						</li>
						
						<?php $num = 1;

						if ( $attachment_ids && $product->get_image_id() ) {
							foreach ( $attachment_ids as $attachment_id ) { ?>

						<li class="custom-open-slide-img" data-open="<?php echo $num; ?>">
							<img src="<?php echo wp_get_attachment_image_url( $attachment_id, 'thumbnail'); ?>" alt="<?php the_title(); ?>" class="flex-active" draggable="false">
						</li>

						<?php $num++; } } ?>

					</ol>
					
				</div>
			
				<div class="row wrap-mobile-thumbs">
			
					<ol class="mobile-thumbs" style="width: auto; position: relative; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">

							<li class="custom-open-slide-img active thumb" data-open="0">
								<img src="<?php echo wp_get_attachment_image_url( $post_thumbnail_id, 'thumbnail'); ?>" alt="<?php the_title(); ?>" class="flex-active" draggable="false">
							</li>

							<?php $num = 1;

							if ( $attachment_ids && $product->get_image_id() ) {
								foreach ( $attachment_ids as $attachment_id ) { ?>

							<li class="custom-open-slide-img thumb" data-open="<?php echo $num; ?>">
								<img src="<?php echo wp_get_attachment_image_url( $attachment_id, 'thumbnail'); ?>" alt="<?php the_title(); ?>" class="flex-active" draggable="false">
							</li>

							<?php $num++; } } ?>

						</ol>
			
					</div>
					
				<div class="summary entry-summary">
		
					<div class="wrap-summary row">
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */

					woocommerce_template_single_title();
					woocommerce_template_single_rating();
					?>

					<div class="row short-desc">	

					<?php echo wp_trim_words(get_the_content(), 20); ?>

					<br /><a href="<?php the_permalink(); ?>" class="view-full-desc">
						Full description	
					</a>	

					</div>

					<?php if(is_user_logged_in()) {
						woocommerce_template_single_price();
					} else {
						echo '<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">Register/Sign in as a trade member to see prices</bdi></span></p>';
					} ?>	

					<div class="row product-meta">

						<?php $productID = $featprod; ?>

						<div class="skus">
							<?php echo $product->get_sku(); ?>
						</div>

						<div class="prod-col">
							<?php the_terms($productID, 'yith_product_brand'); ?>
						</div>

					</div>	

					<div class="row prod-tags">
						Tags: <?php the_terms($productID,'product_cat'); ?>	
					</div>	

					<?php //echo do_shortcode('[addtoany]'); ?>		
						
					<?php if(is_user_logged_in())	{ ?>
					<?php woocommerce_template_single_add_to_cart(); ?>
					<?php } else { ?>
					<div class="wrap-sign-up-button">
					<a href="/register-for-trade-membership/" class="button lightgreen more-marg no-rad">Login or register</a>
					</div>
					<?php } ?>	

					</div>
					
				</div>
			
			</div>

		<?php endwhile; wp_reset_query() // end of the loop. ?>

	</div>	
		
</div>