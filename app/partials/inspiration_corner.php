<?php $insBg = get_field('background_image_insp', 'option'); ?> 
<div class="row insp-corner" style="background-image: url(<?php echo $insBg; ?>);">

	<div class="inside">
		
		<div class="marg">
		
			<div class="sixty">
			
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="Di Designs" class="logo-insp">
				
				<?php the_field('content', 'option'); ?>
				
				<?php 
				$link = get_field('link', 'option');
				if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
				<div class="button-wrap">
						<a class="button lightgreen" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				</div>
				<?php endif; ?>
				
			</div>
			
		</div>
		
	</div>
	
	<img src="<?php the_field('image_insp', 'option'); ?>" class="abs-image-insp">
	
</div>