<div class="row page-content stnd-content">

	<div class="inside">
    
			<div class="row filter-collections">
			
				<form action="<?php the_permalink(); ?>" method="get">
			
				<?php $letter = $_GET['letter']; ?>
			
				<div class="letter <?php if(!$letter) { echo 'active'; } ?>">
					All
				</div>
			
				<?php
				foreach (range('a', 'z') as $column){ ?>
					
					<?php $terms = get_terms( "download_category", array( 'name__like' => $column ) ); ?>
					
					<div class="letter <?php if(!$terms) { echo 'disabled'; } if($column === $letter) { echo 'active'; } ?>"><span><?php echo $column; ?></span></div>
					
      	<?php } ?>
					
				<input type="hidden" name="letter" id="letter">	
					
			</form>
				
		</div>	
				
    	<div class="row">
				
				<div class="row dl-info">
					Please note: To download, you must be <a href="/my-account/">logged in</a> to your trade account. Don’t have an account? <a href="/my-account/">Register now</a>
				</div>
					
				<?php
				if($letter) {
				$termsAll = get_terms(array(
					'taxonomy' => 'download_category',
					'name__like' => $letter,
					'hide_empty' => true
				));
				}	else {	
				$termsAll = get_terms(array(
					'taxonomy' => 'download_category',
					'hide_empty' => true
				));
				}
				?>
				
				<?php
				$page = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
 
				// number of tags to show per-page
				$per_page = 2;

				//count total number of terms related to passed taxonomy
				$number_of_series = count(get_terms('download_category'));
				$offset = ( $page - 1 ) * $per_page;
				?>
				
				<?php
				
				if($letter) {
				$terms = get_terms(array('taxonomy' => 'download_category', 'hide_empty' => true, 'number' => $per_page,
    		'offset' => $offset, 'name__like' => $letter )); 
				} else {

				$terms = get_terms(array('taxonomy' => 'download_category', 'hide_empty' => true, 'number' => $per_page,
    		'offset' => $offset )); 
				} ?>
				
				<div class="row products">
				
				<?php $num = 0; foreach($terms as $term) { ?>
				
				<div class="row download-cat">
				
					<h3><?php echo $term->name; ?></h3>
					
				<?php if($num === 0) { ?>
						<div class="row flexer-type dl-type-row">
							
							<div class="dl-title">
							</div>
							
							<div class="dl-type label">	
								Download Type
							</div>
							
							<div class="dl-download white">
								Download
							</div>
							
						</div>
					<?php } ?>
									
					<?php 
					$args = array ( 'post_type' => 'download', 'posts_per_page' => -1, 'tax_query' => array(array(
					
						'taxonomy' => 'download_category',
						'terms' => $term->name,
						'field' => 'slug',
						
					)) );
					query_posts( $args ); ?>

					<?php while ( have_posts() ) : the_post(); ?>
					
					<div class="row">
						
						<div class="row flexer-type">
						
							<div class="dl-title">
								<?php the_title(); ?>
							</div>
							
							<div class="dl-type">
								<?php the_field('type'); ?>								
							</div>
							
							<div class="dl-download">
							
								<?php if(is_user_logged_in()) { ?>
								
									<a href="<?php the_field('file'); ?>" download>
										Download
									</a>
								
								<?php } else { ?>
 								
									<div class="greyed">
										Download
									</div>
								
								<?php } ?>
								
							</div>
							
						</div>
						
					</div>
					
					<?php endwhile;  wp_reset_query(); ?>
					
				</div>
				
				<?php $num++; } ?>
				</div>
				
				<?php if(count($termsAll) > 2) { ?>
				<div class="row center pager-dls">

						<div class="page-load-status">
							<div class="loader-ellips infinite-scroll-request">

							</div>
						</div>

						<div class="view-more-button button lightgreen">
							Load More
						</div>

						<div class="row showing">
							Showing <span>02</span> of <?php echo count($termsAll); ?> collections 
						</div>

					</div>
				<?php } ?>

				<?php if(!is_paged() || $paged === 0 ) { ?>

					<a class="page-numbers next" href="<?php the_permalink(); ?>/page/2">
					</a>

				<?php } ?>

				<div class="hidden-pager">
				<?php
				$big = 999999999;

					//your function for pagination or plugin you added for pagination

					echo paginate_links(array(
						'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
						'format' => '?paged=%#%',
						'current' => $paged,
						'prev_next' => true,
						'show_all' => true,
						'total' => ceil($number_of_series / $per_page) // like 10 items per page
						));

				?>
				</div>
				
      </div>
        
  	</div>
    <!-- .inside -->
    
</div>
<!-- #page-content -->