<div class="row all-collections">

	<div class="inside">
		
		<div class="row filter-collections">
			
				<form action="<?php the_permalink(); ?>" method="get">
			
				<?php $letter = $_GET['letter']; ?>
			
				<div class="letter <?php if(!$letter) { echo 'active'; } ?>">
					All
				</div>
			
				<?php
				foreach (range('a', 'z') as $column){ ?>
					
					<?php $terms = get_terms( "yith_product_brand", array( 'name__like' => $column ) ); ?>
					
					<div class="letter <?php if(!$terms) { echo 'disabled'; } if($column === $letter) { echo 'active'; } ?>"><span><?php echo $column; ?></span></div>
					
      	<?php } ?>
					
				<input type="hidden" name="letter" id="letter">	
					
			</form>
				
		</div>
		
		<div class="marg">
			
			<div class="row products">
		
			<?php
			$page = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
 
			// number of tags to show per-page
			$per_page = 8;

			//count total number of terms related to passed taxonomy
			$number_of_series = count(get_terms('yith_product_brand'));
			$offset = ( $page - 1 ) * $per_page;
			
			?>
			
			<?php
			if($letter) {
			$termsAll = get_terms(array(
				'taxonomy' => 'yith_product_brand',
				'name__like' => $letter
			));
			}	else {	
			$termsAll = get_terms(array(
				'taxonomy' => 'yith_product_brand'
			));
			}
			?>
				
			<?php 
			if($letter) {
			$terms = get_terms(array(
				'taxonomy' => 'yith_product_brand',
				'number' => $per_page,
    		'offset' => $offset,
				'name__like' => $letter
			));
			} else {
				
			$terms = get_terms(array(
				'taxonomy' => 'yith_product_brand',
				'number' => $per_page,
    		'offset' => $offset
			)
			); 
			}
				
			foreach($terms as $term) { ?>
			
			<?php $termID = $term->term_id;
					$termMeta = get_term_meta($termID); 
					$termImage = $termMeta['thumbnail_id'][0];
					$termGetImage = wp_get_attachment_image_url($termImage, 'medium_large');
				?>
			
			<div class="half collection-item">
			
				<a class="col-grid-item row collections-slider-link" href="<?php echo get_term_link($termID); ?>">

						<span class="img-wrap">
									
							<img src="<?php echo $termGetImage; ?>" alt="<?php echo $term->name; ?>">

						</span>
					
						<span class="collection-slider-title">

								<?php echo $term->name; ?>
										
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg>
										
							</span>

					</a>
				
			</div>
			
			<?php } ?>
				
			</div>
			
			<?php if(count($termsAll) > 8) { ?>
			<div class="row center">
								
					<div class="page-load-status">
						<div class="loader-ellips infinite-scroll-request">
											
						</div>
					</div>
									
					<div class="view-more-button button lightgreen">
						Load More
					</div>
				
					<div class="row showing">
						Showing <span>08</span> of <?php echo count($termsAll); ?> collections 
					</div>
									
				</div>
			<?php } ?>
			
			<?php if(!is_paged() || $paged === 0 ) { ?>
				
				<a class="page-numbers next" href="<?php the_permalink(); ?>/page/2">
				</a>
				
			<?php } ?>
			
			<div class="hidden-pager">
			<?php
			$big = 999999999;
 
        //your function for pagination or plugin you added for pagination
        
        echo paginate_links(array(
          'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
          'format' => '?paged=%#%',
          'current' => $paged,
					'prev_next' => true,
					'show_all' => true,
          'total' => ceil($number_of_series / $per_page) // like 10 items per page
          ));
			
			?>
			</div>
			
		</div>
		
	</div>
	
</div>