<div class="row column-content stnd-content">

	<div class="inside">
    
    	<div class="row cols-2">
				
				<?php the_sub_field('content'); ?>
				
      </div>
        
  	</div>
    <!-- .inside -->
    
</div>
<!-- #page-content -->