<div class="row page-content collections-slider">
	
	<div class="row collections-slider-inner">
	
		<div class="inside">

			<div class="row wrap-collections-slider-intro">
				
				<h2>Collections</h2>

				<?php the_sub_field('introduction'); ?>

				<div class="collections-slider-buttons">
				
					<div class="go-to-prev">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M17.961,8.588,4.315,8.609l3.891-3.9a.986.986,0,0,0-1.4-1.393L1.237,8.9a.893.893,0,0,0-.065.073.381.381,0,0,0-.024.032c-.012.015-.023.03-.034.046s-.016.026-.024.04-.017.028-.025.042a.4.4,0,0,0-.02.042c-.008.016-.015.03-.022.046l-.014.041C1,9.281,1,9.3.991,9.314s-.007.027-.01.04a.529.529,0,0,0-.013.054c0,.014-.005.029-.007.043S.955,9.486.954,9.5s0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s.005.033.007.05,0,.03.007.045.009.035.013.052.007.028.011.042.011.033.017.049.01.028.016.042.013.029.02.044.013.029.021.044l.024.04c.009.014.017.029.026.042s.021.029.031.043.017.024.026.035.043.049.066.072l5.587,5.572a.986.986,0,0,0,1.393-1.4l-3.9-3.89,13.646-.021a.987.987,0,1,0,0-1.973Z"/></svg>
					</div>

					<div class="go-to-next">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg>
					</div>
					
				</div>
				
			</div>	
				
			<div class="row wrap-videos">

					<div class="marg no-flex">

						<div class="row make-v-slider">

							<?php $terms = get_terms(
									'yith_product_brand'
							); ?>

							<?php foreach($terms as $term) { ?>

							<?php $termID = $term->term_id;
							$termMeta = get_term_meta($termID); 
							$termImage = $termMeta['banner_id'][0];
							$termGetImage = wp_get_attachment_image_url($termImage, 'rect-bigger');
							if($termGetImage) {
							?>

							<a class="collections-slider-link" href="<?php echo get_term_link($termID); ?>">

								<span class="img-wrap">
									
									<img src="<?php echo $termGetImage; ?>" alt="<?php echo $term->name; ?>">

									<span class="collection-slider-title">

										<?php echo $term->name; ?>
										
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg>
										
									</span>
									
								</span>

							</a>

							<?php } } ?>

						</div>

				</div>

			</div>					

		</div>
		
	</div>
	
</div>