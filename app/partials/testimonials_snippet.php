<?php $bg = get_sub_field('background_image'); ?>
<div class="test-snippet row" style="background-image: url(<?php echo $bg; ?>);">

	<div class="inside">
	
		<div class="marg">
		
			<div class="half">
			
				<h2>Testimonials</h2>
				
				<?php the_sub_field('introduction'); ?>
				
				<div class="row button-wrap">
					<a href="/about-us/testimonials" class="button lightgreen">Testimonials</a>
				</div>
					
			</div>
			
			<div class="half">
			
				<div class="row tester-snippet">
					<?php the_sub_field('testimonial'); ?>
				</div>
				
				<div class="row tester-snippet-caption">
					<?php the_sub_field('caption'); ?>
				</div>
				
			</div>
			
		</div>
	
	</div>
	
</div>