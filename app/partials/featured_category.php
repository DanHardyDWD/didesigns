<div class="row feat-prod featured-category">

	<div class="inside">
		
		<div class="row main-shop flexer">
						
			<div class="woo-main alt">
			
				<?php 
				$featprodcat = get_sub_field('product_category');
				$args = array ( 'post_type' => 'product', 'posts_per_page' => 3, 'orderby' => 'rand', 'tax_query' => array(
				
					array(
						'taxonomy' => 'product_cat',
            'field'    => 'term_id',
            'terms'    => array( $featprodcat )
					)
				
				) );
				query_posts( $args ); ?>
				
				<div class="row woocommerce">
				
					
					<?php woocommerce_product_loop_start(); while ( have_posts() ) : ?>
					<?php the_post(); ?>

					<?php
					do_action( 'woocommerce_shop_loop' );

					wc_get_template_part( 'content', 'product' );

					?>

					<?php endwhile; wp_reset_query() // end of the loop. ?>
					
					
					<?php woocommerce_product_loop_end(); ?>
				
				</div>
				
			</div>
			
			<div class="woo-side alt-main">		
				
				<?php $term_name = get_term( $featprodcat )->name; ?>
				
				<h2><?php echo $term_name; ?></h2>
				
				<?php echo term_description($featprodcat); ?>
				
				<div class="row button-wrap">
					<a href="<?php echo get_term_link($featprodcat); ?>" class="button lightgreen">Shop <?php echo $term_name; ?></a>
				</div>
					
			</div>
				
		</div>
				
	</div>
	
</div>