<div class="row page-content stnd-content">

	<div class="inside">
    
    	<div class="row">
				
				<?php the_sub_field('content'); ?>
				
      </div>
        
  	</div>
    <!-- .inside -->
    
</div>
<!-- #page-content -->