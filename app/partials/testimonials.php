<div class="row page-content stnd-content">

	<div class="inside">
    
    	<div class="row">
				
				<div class="marg testers">
					
					<?php while(has_sub_field('testimonials')): ?>
					
					<div class="half">
					
						<div class="row testimonial-box">
						
							<div class="row tester-content">
								<?php the_sub_field('testimonial'); ?>
							</div>
							
							<div class="row tester-person">
							
								<strong><?php the_sub_field('person'); ?></strong><br />
								<?php the_sub_field('position'); ?>
								
							</div>
								
						</div>
						
					</div>
					
					<?php endwhile; ?>
					
				</div>
				
      </div>
        
  	</div>
    <!-- .inside -->
    
</div>
<!-- #page-content -->