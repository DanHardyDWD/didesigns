<div class="row page-content stnd-content h-img-h-content">

	<div class="inside">
    
    	<div class="row">
				
				<div class="marg center">
					
					<div class="half">
					
						<?php
							$image = get_sub_field('image');
							if( $image ):

							$url = $image['url'];
							$title = $image['title'];
							$alt = $image['alt'];
							$caption = $image['caption'];

									// Thumbnail size attributes.
							$size = 'square';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];

							?>

							<img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" />

						<?php endif; ?>
						
					</div>
					
					<div class="half">
					
						<?php the_sub_field('content'); ?>
						
					</div>
					
				</div>
				
      </div>
        
  	</div>
    <!-- .inside -->
    
</div>
<!-- #page-content -->