<div class="row hero alt">

	<div class="slider row">
	
	<?php $number = 0 ;while(has_sub_field('slides')): ?>	
		
		<div class="row slide">
		
			<div class="inside">

				<div class="marg">

					<div class="fourty">

						<?php if($number === 0) { ?>
						<h1><?php the_sub_field('title'); ?></h1>
						<?php } else { ?>
						<h2><?php the_sub_field('title'); ?></h2>
						<?php } ?>
						
						<?php the_sub_field('content'); ?>

						<div class="row button-wrap">
						
						<?php $num = 0; while(has_sub_field('buttons')): ?>
						
						<?php 
						$link = get_sub_field('button', 'option');
						if( $link ): 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
								?>
								<a class="button <?php if ($num % 2 != 0) { echo 'green'; } else { echo 'lightgreen'; } ?>" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
						
						<?php $num++; endwhile; ?>
						</div>
						
					</div>

				</div>

			</div>
			
			<img src="<?php the_sub_field('image'); ?>" alt="<?php the_sub_field('title'); ?>" class="hero-image">
		
		</div>
		<?php $number++; endwhile; ?>
		
	</div>
	
</div>