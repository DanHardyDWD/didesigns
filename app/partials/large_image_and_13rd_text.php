<div class="row page-content large-img-w-content">
	
	<div class="inside">
	
		<div class="marg">
		
			<div class="seventy">
			</div>
			
			<div class="thirty">
				
				<?php the_sub_field('content'); ?>
				
				<?php 
				$link = get_field('link', 'option');
				if( $link ): 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						?>
				<div class="row button-wrap">
						<a class="button lightgreen" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				</div>
				<?php endif; ?>
				
			</div>
			
		</div>
		
	</div>
	
	<?php
			$image = get_sub_field('image');
			if( $image ):
			$url = $image['url'];
			$title = $image['title'];
			$alt = $image['alt'];
			$caption = $image['caption'];
			$size = 'large';
			$thumb = $image['sizes'][ $size ];
			$width = $image['sizes'][ $size . '-width' ];
			$height = $image['sizes'][ $size . '-height' ];

		?>

		<img src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($alt); ?>" />

		<?php endif; ?>
	
</div>