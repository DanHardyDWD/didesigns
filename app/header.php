<?php
/**
 * Header template for the theme
 *
 * Displays all of the <head> section and everything up till <div id="main">.
 *
 * @package WordPress
 * @subpackage Di_Designs
 * @since Di Designs 1.0
 */
?><!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title(); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?ver=1.2.7" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
<link rel="stylesheet" href="https://use.typekit.net/nvc2sog.css">
	<link rel="stylesheet" href="flexslider.css" type="text/css">
</head>

<body <?php body_class(); ?>>	
	
<div class="row menu-h">
	
	<ul>
		<?php wp_nav_menu( array('items_wrap' => '%3$s', 'container' => false, 'theme_location' => 'primary', 'sort_column' => 'menu_order', 'after' => '<span class="open-sub-sub"></span>') ); ?>
	</ul>  
	
	<div class="row c-links">
					
						<?php $tel = get_field('phone_number', 'option'); if($tel) { ?>
							<a href="tel<?php echo $tel; ?>">
								
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#435053;}</style></defs><path class="a" d="M13.954,18.746c-2.643,0-6.03-2.368-8.318-4.584C2.6,11.225.794,8.019.794,5.586.794,2.744,3.564.437,5.372.437a1.7,1.7,0,0,1,1.473.839L8.56,4.135a1.705,1.705,0,0,1-.4,2.214L6.445,7.726a17.045,17.045,0,0,0,5.369,5.37l1.376-1.717a1.707,1.707,0,0,1,2.213-.4L18.266,12.7a1.7,1.7,0,0,1,.838,1.472C19.1,15.976,16.8,18.746,13.954,18.746ZM5.372,1.581A3.543,3.543,0,0,0,3.206,2.76,4.179,4.179,0,0,0,1.938,5.586c0,2.1,1.722,5.069,4.494,7.754,2.673,2.589,5.626,4.262,7.522,4.262a4.183,4.183,0,0,0,2.816-1.26,3.538,3.538,0,0,0,1.189-2.173.567.567,0,0,0-.279-.49l-2.863-1.717a.562.562,0,0,0-.736.135L12.393,14.2a.573.573,0,0,1-.736.136A18.18,18.18,0,0,1,5.2,7.884a.574.574,0,0,1,.135-.737L7.445,5.458a.564.564,0,0,0,.133-.736L5.862,1.863A.566.566,0,0,0,5.372,1.581Z"/></svg>
								
								<span><?php echo $tel; ?></span>
								
							</a>
						<?php } ?>
						
						<?php $email = get_field('email_address', 'option'); if($email) { ?><br/>
							<a href="mailto<?php echo $email; ?>" class="email">
								
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#435053;}</style></defs><path class="a" d="M16.323,2.686H3.574A2.127,2.127,0,0,0,1.45,4.811v9.561A2.127,2.127,0,0,0,3.574,16.5H16.323a2.127,2.127,0,0,0,2.125-2.125V4.811A2.127,2.127,0,0,0,16.323,2.686Zm1.063,11.686a1.064,1.064,0,0,1-1.063,1.063H3.574a1.064,1.064,0,0,1-1.062-1.063V4.811A1.064,1.064,0,0,1,3.574,3.748H16.323a1.064,1.064,0,0,1,1.063,1.063Z"/><path class="a" d="M16.185,4.984a.53.53,0,0,0-.75-.035L10.764,9.2l0,0-.1.089a1.058,1.058,0,0,1-1.43,0l-.1-.09h0L4.463,4.949a.531.531,0,0,0-.715.786L7.991,9.591,3.748,13.448a.53.53,0,0,0-.035.75.531.531,0,0,0,.75.036L8.8,10.289a2.11,2.11,0,0,0,2.294,0l4.339,3.945a.531.531,0,0,0,.715-.786L11.907,9.591,16.15,5.735A.531.531,0,0,0,16.185,4.984Z"/></svg>
								
								<span><?php echo $email; ?></span>
								
							</a>
						<?php } ?>
						
					</div>
					
					<div class="row social">
					
						<?php while(has_sub_field('social_media_networks','option')): ?>

						<?php $attachment_id_icon = get_sub_field('icon'); ?>					

						<?php $url = get_sub_field('url','option'); ?>

						<a target="_blank" href="<?php echo $url; ?>" title="<?php the_sub_field('name'); ?>"><i class="fa <?php echo $attachment_id_icon; ?>"></i></a>

						<?php endwhile;  ?>
						
					</div>
	
</div>	
	
<header class="row">

	<?php $notice = get_field('notice', 'option'); if($notice) { ?>
	
	<div class="row notice">
	
		<?php echo $notice; ?>
		
	</div>
	
	<?php } ?>
	
	<div class="head-main row">
	
		<div class="inside">

			<a href="#" class="nav mobile-open">
				
					<span></span>
					<span></span>
					<span></span>
					<span></span>
					
			</a>
			
			<div class="row head-top">
			
				<div class="logo">
				
					<a href="<?php echo home_url(); ?>">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 366 47"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M5.5,7.211h7.953c5.792,0,9.884.689,13.47,4.183,2.988,2.9,4.781,7.493,4.781,12.734,0,5.655-2.115,10.343-5.655,13.1-3.769,2.942-7.677,2.987-12.5,2.987H5.5Zm6.3,6.16V34.058h3.31c3.449,0,5.885-.46,7.816-2.529a11.136,11.136,0,0,0,2.436-7.538c0-6.9-3.815-10.62-10.988-10.62Z"/><path class="a" d="M54.22,37.321a3.5,3.5,0,0,1-3.493,3.541,3.572,3.572,0,0,1-3.541-3.541,3.517,3.517,0,0,1,7.034,0Z"/><path class="a" d="M68.605,7.211h6.3V40.218h-6.3Z"/><path class="a" d="M96.228,37.321a3.5,3.5,0,0,1-3.493,3.541,3.572,3.572,0,0,1-3.54-3.541,3.517,3.517,0,0,1,7.033,0Z"/><path class="a" d="M118.15,7.211h8.137c3.631,0,6.988.184,10.252,1.517,5.93,2.39,9.469,8.09,9.469,15.355,0,9.423-5.7,16.135-16.32,16.135H118.15ZM126.333,37c6.251,0,10.39-.552,13.332-4.136a13.516,13.516,0,0,0,2.9-8.736,14.064,14.064,0,0,0-2.758-8.735c-3.126-4.091-7.816-4.964-14.987-4.964H121.46V37Z"/><path class="a" d="M162.823,7.211H181.67v3.218H166.133V20.772H181.67v3.219H166.133V37H181.67v3.218H162.823Z"/><path class="a" d="M208.236,13.831c-1.795-2.345-3.357-4.046-6.068-4.046a4.185,4.185,0,0,0-4.461,4.183,6.673,6.673,0,0,0,2.944,5.1c6.2,4.6,10.756,7.77,10.756,13.1a9.092,9.092,0,0,1-9.286,8.873c-4.551,0-7.538-2.529-9.975-6.988l2.805-1.7c1.975,3.631,4.367,5.471,6.894,5.471a5.844,5.844,0,0,0,6.113-5.608c0-3.633-3.4-5.7-10.893-11.768a8.642,8.642,0,0,1-2.805-6.345,7.664,7.664,0,0,1,8-7.724c4.046,0,6.481,2.437,8.644,5.379Z"/><path class="a" d="M230.707,7.211h3.31V40.218h-3.31Z"/><path class="a" d="M285.821,24.359c-.047,10.48-6.712,16.64-16.6,16.64-11.448,0-18.618-8.182-18.618-17.284,0-9.332,7.262-17.332,18.11-17.332,6.069,0,10.712,2.208,15.126,6.391l-2.529,2.39A18.245,18.245,0,0,0,268.81,9.6c-7.906,0-14.755,6.068-14.755,14.022,0,7.585,6.389,14.342,15.263,14.342,6.252,0,11.86-3.723,12.778-10.388H271.569V24.359Z"/><path class="a" d="M300.289,40.218V7.211h.689L322.861,32.5V7.211h3.31V40.218h-.736L303.6,15.256V40.218Z"/><path class="a" d="M357.329,13.831c-1.793-2.345-3.357-4.046-6.068-4.046a4.183,4.183,0,0,0-4.459,4.183,6.67,6.67,0,0,0,2.942,5.1c6.2,4.6,10.756,7.77,10.756,13.1a9.092,9.092,0,0,1-9.286,8.873c-4.551,0-7.538-2.529-9.976-6.988l2.806-1.7c1.977,3.631,4.367,5.471,6.894,5.471a5.845,5.845,0,0,0,6.115-5.608c0-3.633-3.4-5.7-10.895-11.768a8.648,8.648,0,0,1-2.8-6.345,7.663,7.663,0,0,1,8-7.724c4.044,0,6.481,2.437,8.642,5.379Z"/></svg>
					</a>
					
				</div>
				
				<div class="head-right">
					
					<div class="c-links">
						
						<?php $tel = get_field('phone_number', 'option'); if($tel) { ?>
							<a href="tel<?php echo $tel; ?>">

								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#435053;}</style></defs><path class="a" d="M13.954,18.746c-2.643,0-6.03-2.368-8.318-4.584C2.6,11.225.794,8.019.794,5.586.794,2.744,3.564.437,5.372.437a1.7,1.7,0,0,1,1.473.839L8.56,4.135a1.705,1.705,0,0,1-.4,2.214L6.445,7.726a17.045,17.045,0,0,0,5.369,5.37l1.376-1.717a1.707,1.707,0,0,1,2.213-.4L18.266,12.7a1.7,1.7,0,0,1,.838,1.472C19.1,15.976,16.8,18.746,13.954,18.746ZM5.372,1.581A3.543,3.543,0,0,0,3.206,2.76,4.179,4.179,0,0,0,1.938,5.586c0,2.1,1.722,5.069,4.494,7.754,2.673,2.589,5.626,4.262,7.522,4.262a4.183,4.183,0,0,0,2.816-1.26,3.538,3.538,0,0,0,1.189-2.173.567.567,0,0,0-.279-.49l-2.863-1.717a.562.562,0,0,0-.736.135L12.393,14.2a.573.573,0,0,1-.736.136A18.18,18.18,0,0,1,5.2,7.884a.574.574,0,0,1,.135-.737L7.445,5.458a.564.564,0,0,0,.133-.736L5.862,1.863A.566.566,0,0,0,5.372,1.581Z"/></svg>

								<span><?php echo $tel; ?></span>

							</a>
						<?php } ?>

						<?php $email = get_field('email_address', 'option'); if($email) { ?>
							<a href="mailto<?php echo $email; ?>" class="email">

								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#435053;}</style></defs><path class="a" d="M16.323,2.686H3.574A2.127,2.127,0,0,0,1.45,4.811v9.561A2.127,2.127,0,0,0,3.574,16.5H16.323a2.127,2.127,0,0,0,2.125-2.125V4.811A2.127,2.127,0,0,0,16.323,2.686Zm1.063,11.686a1.064,1.064,0,0,1-1.063,1.063H3.574a1.064,1.064,0,0,1-1.062-1.063V4.811A1.064,1.064,0,0,1,3.574,3.748H16.323a1.064,1.064,0,0,1,1.063,1.063Z"/><path class="a" d="M16.185,4.984a.53.53,0,0,0-.75-.035L10.764,9.2l0,0-.1.089a1.058,1.058,0,0,1-1.43,0l-.1-.09h0L4.463,4.949a.531.531,0,0,0-.715.786L7.991,9.591,3.748,13.448a.53.53,0,0,0-.035.75.531.531,0,0,0,.75.036L8.8,10.289a2.11,2.11,0,0,0,2.294,0l4.339,3.945a.531.531,0,0,0,.715-.786L11.907,9.591,16.15,5.735A.531.531,0,0,0,16.185,4.984Z"/></svg>

								<span><?php echo $email; ?></span>

							</a>
						<?php } ?>
						
						<div class="account-link more-left">
						
							<?php if(is_user_logged_in()) { ?>
							
								<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>">
							
									<span>My account</span>	
									
							<?php } else { ?>
									
								<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','woothemes'); ?>">
									
									<span>Account Login</span>
									
							<?php } ?>
									
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M9.949,10.753A5.227,5.227,0,1,0,4.722,5.526,5.233,5.233,0,0,0,9.949,10.753Zm0-9.292A4.065,4.065,0,1,1,5.884,5.526,4.069,4.069,0,0,1,9.949,1.461Z"/><path class="a" d="M14.6,11.914H5.3A4.071,4.071,0,0,0,1.237,15.98V18.3a.581.581,0,0,0,1.162,0V15.98a2.908,2.908,0,0,1,2.9-2.9H14.6a2.908,2.908,0,0,1,2.9,2.9V18.3a.581.581,0,0,0,1.162,0V15.98A4.071,4.071,0,0,0,14.6,11.914Z"/></svg>
									
								</a>
							
						</div>
						
						<div class="account-link">
						
								<a href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('My Account','woothemes'); ?>">
							
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M16.849,4.416H13.4V3.841a3.45,3.45,0,1,0-6.9,0v.575H3.049a.575.575,0,0,0-.575.575V17.067A1.727,1.727,0,0,0,4.2,18.792H15.7a1.727,1.727,0,0,0,1.725-1.725V4.991A.575.575,0,0,0,16.849,4.416Zm-9.2-.575a2.3,2.3,0,0,1,4.6,0v.575h-4.6Zm8.625,13.226a.576.576,0,0,1-.575.575H4.2a.576.576,0,0,1-.575-.575V5.566H6.5V6.815a1.725,1.725,0,1,0,1.15,0V5.566h4.6V6.815a1.725,1.725,0,1,0,1.15,0V5.566h2.875Zm-9.2-9.2a.575.575,0,1,1-.575.575A.576.576,0,0,1,7.074,7.866Zm5.75,0a.575.575,0,1,1-.575.575A.576.576,0,0,1,12.824,7.866Z"/></svg>
									
									<span class="cart-num">
									
										<span class="cart-count">
										<?php
										global $woocommerce;
										echo $woocommerce->cart->cart_contents_count;
										?>
										</span>
									
									</span>
									
								</a>
							
						</div>
						
					</div>
					
				</div>
				
			</div>
			
			<div class="row head-bottom">
			
				<div class="main-menu">
					<ul><?php wp_nav_menu(array('theme_location' => 'primary', 'container' => '', 'walker' => new Dan_Walker())); ?></ul>
				</div>
				
				<div class="head-right">
					
					<div class="social">
					
						<?php while(has_sub_field('social_media_networks','option')): ?>

						<?php $attachment_id_icon = get_sub_field('icon'); ?>					

						<?php $url = get_sub_field('url','option'); ?>

						<a target="_blank" href="<?php echo $url; ?>" title="<?php the_sub_field('name'); ?>"><i class="fa <?php echo $attachment_id_icon; ?>"></i></a>

						<?php endwhile;  ?>
						
					</div>
					
					<div class="head-search-wrap">
						<?php get_search_form(); ?>
					</div>
					
				</div>
				
			</div>
			
		</div>
		<!-- .inside -->
		
	</div>
    
</header>
	
<div class="wrap-site row">