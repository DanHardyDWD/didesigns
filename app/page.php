<?php
/**
 * Template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Di_Designs
 * @since Di Designs 1.0
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php $showHero = get_field('show_hero_area'); if(!is_woocommerce() && !is_cart() && !is_checkout() && !is_account_page()) { ?>

<?php if($showHero) { ?>
<div class="hero-and-crumbs row">

	<div class="hero-top row">

		<img src="<?php the_post_thumbnail_url('large'); ?> ?>" class="abs-image-insp" alt="<?php the_title(); ?>">
		
		<div class="inside">
		
			<?php $title = get_field('custom_page_title'); if($title) { $pageTitle = $title; } else { $pageTitle = get_the_title(); } ?>
			
			<h1><?php echo $pageTitle; ?></h1>
			
		</div>
		
	</div>
	
	<div class="crumbs row">
	
		<div class="inside">
			<?php wordpress_breadcrumbs(); ?>
		</div>
		
	</div>
	
</div>
<?php } ?>

<div class="wrap-blocks row">
<?php
	// Loop through rows.
$blockNum = 0; while ( have_rows('page_builder') ) : the_row();
  $fileToGet = get_row_layout().'.php';
	include(locate_template('partials/'.$fileToGet));
	
$blockNum++; endwhile; ?>
</div>

<?php } else { ?>

<?php if($showHero && has_post_thumbnail()) { ?>
<div class="hero-and-crumbs row">

	<div class="hero-top row">

		<img src="<?php the_post_thumbnail_url('large'); ?> ?>" class="abs-image-insp" alt="<?php the_title(); ?>">
		
		<div class="inside">
		
			<?php $title = get_field('custom_page_title'); if($title) { $pageTitle = $title; } else { $pageTitle = get_the_title(); } ?>
			
			<h1><?php echo $pageTitle; ?></h1>
			
		</div>
		
	</div>
	
	<div class="crumbs row">
	
		<div class="inside">
			<?php wordpress_breadcrumbs(); ?>
		</div>
		
	</div>
	
</div>
<?php } ?>

<div class="row page-content stnd-content">

	<div class="inside">
    
    	<div class="row">
				
				<h1><?php the_title(); ?></h1>
				
				<?php the_content(); ?>
				
      </div>
        
  	</div>
    <!-- .inside -->
    
</div>
<!-- #page-content -->
<?php } ?>

<?php endwhile; ?>

<?php get_footer(); ?>