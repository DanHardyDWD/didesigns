<?php
/**
 * Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Di_Designs
 * @since Di Designs 1.0
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<div class="row page-content">

	<div class="inside">
    
    	<div class="row">
				
				<div class="row crumbs-inner">
				
					<?php wordpress_breadcrumbs(); ?>
					
				</div>
				
				<h1><?php the_title(); ?></h1>
				
      </div>
        
  	</div>
    <!-- .inside -->
    
		<div class="wrap-blocks row">
				<?php
					// Loop through rows.
				$blockNum = 0; while ( have_rows('post_builder') ) : the_row();
					$fileToGet = get_row_layout().'.php';
					include(locate_template('partials/'.$fileToGet));

				$blockNum++; endwhile; ?>
				</div>
	
		<div class="row related">
	
			<div class="inside">
			
				<div class="row related-inner">
				
					<h2>Related</h2>
					
					<div class="row">
					
						<div class="marg">
						
							<?php $related = get_posts( 
									array( 
											'category__in' => wp_get_post_categories( $post->ID ), 
											'numberposts'  => 3, 
											'post__not_in' => array( $post->ID ) 
									) 
							); ?>
							
							
							<div class="row flex-this">
							
							<?php $num = 0; foreach( $related as $post ) { ?>
														
								<?php if($num ===0) { ?>
								
								<div class="fourty">
								
									<div class="news-item item-<?php echo $num; ?>">
				
										<div class="img-item-news">
										<a href="<?php the_permalink(); ?>">

											<span class="row img-wrap">
												<?php the_post_thumbnail('rect'); ?>
											</span>

										</a>
										</div>

											<span class="cta-box-info row">

												<div class="row wrap-cats-loop">
													<?php the_category(', '); ?>
												</div>

												<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

												<div class="row blurb">
													<?php echo wp_trim_words(get_the_excerpt(), 20); ?>
												</div>

												<div class="row button-wrap">
													<a href="<?php the_permalink(); ?>" class="green">Read more</a>
												</div>

											</span>

										</a>

									</div>
									
								</div>
								
								<?php } ?>
								
								<?php if($num === 1) { ?>
								
								<div class="sixty">
									
									<div class="news-item wider <?php if($num % 2 == 0){ echo 'flip'; } ?> item-<?php echo $num; ?>">
				
										<div class="img-item-news">

											<a href="<?php the_permalink(); ?>">

												<span class="row img-wrap">
													<?php the_post_thumbnail('blog-square'); ?>
												</span>

											</a>

										</div>

											<div class="wrap-cta">

												<span class="cta-box-info row">

													<div class="row wrap-cats-loop">
														<?php the_category(', '); ?>
													</div>

													<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

													<div class="row blurb">
														<?php echo wp_trim_words(get_the_excerpt(), 20); ?>
													</div>

													<div class="row button-wrap">
														<a href="<?php the_permalink(); ?>" class="green">Read more</a>
													</div>

												</span>

											</div>

										</div>
								
								<?php } ?>
								
								<?php if($num === 2) { ?>
									
									<div class="news-item wider <?php if($num % 2 == 0){ echo 'flip'; } ?> item-<?php echo $num; ?>">
				
										<div class="img-item-news">

											<a href="<?php the_permalink(); ?>">

												<span class="row img-wrap">
													<?php the_post_thumbnail('blog-square'); ?>
												</span>

											</a>

										</div>

											<div class="wrap-cta">

												<span class="cta-box-info row">

													<div class="row wrap-cats-loop">
														<?php the_category(', '); ?>
													</div>

													<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

													<div class="row blurb">
														<?php echo wp_trim_words(get_the_excerpt(), 20); ?>
													</div>

													<div class="row button-wrap">
														<a href="<?php the_permalink(); ?>" class="green">Read more</a>
													</div>

												</span>

											</div>

										</div>
									
									</div>
									
								<?php } ?>
									
							
							<?php $num++; } ?>
							
						</div>
						
					</div>
					
				</div>
			
			</div>
			
		</div>
	
</div>
<!-- #page-content -->
<?php endwhile; ?>

<?php get_footer(); ?>