<?php
/**
 * Di Designs functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, didesigns_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'didesigns_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage Di_Designs
 * @since Di Designs 1.0
 */

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) )
	$content_width = 584;

/*
 * Tell WordPress to run didesigns_setup() when the 'after_setup_theme' hook is run.
 */
add_action( 'after_setup_theme', 'didesigns_setup' );

if ( ! function_exists( 'didesigns_setup' ) ):
/**
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 *
 * To override didesigns_setup() in a child theme, add your own didesigns_setup to your child theme's
 * functions.php file.
 *
 * @uses load_theme_textdomain()    For translation/localization support.
 * @uses add_editor_style()         To style the visual editor.
 * @uses add_theme_support()        To add support for post thumbnails, automatic feed links, custom headers
 * 	                                and backgrounds, and post formats.
 * @uses register_nav_menus()       To add support for navigation menus.
 * @uses register_default_headers() To register the default custom header images provided with the theme.
 * @uses set_post_thumbnail_size()  To set a custom post thumbnail size.
 *
 * @since Di Designs 1.0
 */
function didesigns_setup() {

	/*
	 * Make Di Designs available for translation.
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on Di Designs, use
	 * a find and replace to change 'didesigns' to the name
	 * of your theme in all the template files.
	 */
	load_theme_textdomain( 'didesigns', get_template_directory() . '/languages' );

	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();
	
	// Add default posts and comments RSS feed links to <head>.
	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menu( 'primary', __( 'Primary Menu', 'didesigns' ) );
	register_nav_menu( 'footercopy', __( 'Footer Copyright Menu', 'didesigns' ) );
	register_nav_menu( 'footer1', __( 'Footer Collections Menu', 'didesigns' ) );
	register_nav_menu( 'footer2', __( 'Footer Product types Menu', 'didesigns' ) );
	register_nav_menu( 'footer3', __( 'Footer Company Menu', 'didesigns' ) );
	
	// This theme uses Featured Images (also known as post thumbnails) for per-post/per-page Custom Header images
	add_theme_support( 'post-thumbnails' );

	/*
	 * We'll be using post thumbnails for custom header images on posts and pages.
	 * We want them to be the size of the header image that we just defined.
	 * Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	 */
	set_post_thumbnail_size( $custom_header_support['width'], $custom_header_support['height'], true );

	/*
	 * Add Di Designs's custom image sizes.
	 * Used for large feature (header) images.
	 */
	add_image_size( 'large-feature', $custom_header_support['width'], $custom_header_support['height'], true );
	// Used for featured posts if a large-feature doesn't exist.
	add_image_size( 'small-feature', 500, 300 );
	add_image_size( 'rect', 913, 456, true );
	add_image_size( 'square-blog', 913, 913, true );
	add_image_size( 'rect-bigger', 913, 556, true );
	add_image_size( 'square', 1000, 1000, true );
}
endif; // didesigns_setup

/**
 * Set the post excerpt length to 40 words.
 *
 * To override this length in a child theme, remove
 * the filter and add your own function tied to
 * the excerpt_length filter hook.
 *
 * @since Di Designs 1.0
 *
 * @param int $length The number of excerpt characters.
 * @return int The filtered number of characters.
 */
function didesigns_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'didesigns_excerpt_length' );

if ( ! function_exists( 'didesigns_continue_reading_link' ) ) :
/**
 * Return a "Continue Reading" link for excerpts
 *
 * @since Di Designs 1.0
 *
 * @return string The "Continue Reading" HTML link.
 */
function didesigns_continue_reading_link() {
	return ' <a href="'. esc_url( get_permalink() ) . '">' . __( '', 'didesigns' ) . '</a>';
}
endif; // didesigns_continue_reading_link

/**
 * Replace "[...]" in the Read More link with an ellipsis.
 *
 * The "[...]" is appended to automatically generated excerpts.
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @since Di Designs 1.0
 *
 * @param string $more The Read More text.
 * @return The filtered Read More text.
 */
function didesigns_auto_excerpt_more( $more ) {
	return ' &hellip;' . didesigns_continue_reading_link();
}
add_filter( 'excerpt_more', 'didesigns_auto_excerpt_more' );

/**
 * Add a pretty "Continue Reading" link to custom post excerpts.
 *
 * To override this link in a child theme, remove the filter and add your own
 * function tied to the get_the_excerpt filter hook.
 *
 * @since Di Designs 1.0
 *
 * @param string $output The "Continue Reading" link.
 * @return string The filtered "Continue Reading" link.
 */
function didesigns_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= didesigns_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'didesigns_custom_excerpt_more' );

/**
 * Show a home link for the wp_nav_menu() fallback, wp_page_menu().
 *
 * @since Di Designs 1.0
 *
 * @param array $args The page menu arguments. @see wp_page_menu()
 * @return array The filtered page menu arguments.
 */
function didesigns_page_menu_args( $args ) {
	if ( ! isset( $args['show_home'] ) )
		$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'didesigns_page_menu_args' );

/**
 * Register sidebars and widgetized areas.
 *
 * Also register the default Epherma widget.
 *
 * @since Di Designs 1.0
 */
function didesigns_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Main Sidebar', 'didesigns' ),
		'id' => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'didesigns_widgets_init' );

if ( ! function_exists( 'didesigns_content_nav' ) ) :
/**
 * Display navigation to next/previous pages when applicable.
 *
 * @since Di Designs 1.0
 *
 * @param string $html_id The HTML id attribute.
 */
function didesigns_content_nav( $html_id ) {
	global $wp_query;

	if ( $wp_query->max_num_pages > 1 ) : ?>
		<nav id="<?php echo esc_attr( $html_id ); ?>">
			<h3 class="assistive-text"><?php _e( 'Post navigation', 'didesigns' ); ?></h3>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'didesigns' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'didesigns' ) ); ?></div>
		</nav><!-- #nav-above -->
	<?php endif;
}
endif; // didesigns_content_nav

if ( ! function_exists( 'didesigns_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own didesigns_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Di Designs 1.0
 *
 * @param object $comment The comment object.
 * @param array  $args    An array of comment arguments. @see get_comment_reply_link()
 * @param int    $depth   The depth of the comment.
 */
function didesigns_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'didesigns' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'didesigns' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment">
			<footer class="comment-meta">
				<div class="comment-author vcard">
					<?php
						$avatar_size = 68;
						if ( '0' != $comment->comment_parent )
							$avatar_size = 39;

						echo get_avatar( $comment, $avatar_size );

						/* translators: 1: comment author, 2: date and time */
						printf( __( '%1$s on %2$s <span class="says">said:</span>', 'didesigns' ),
							sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
							sprintf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
								esc_url( get_comment_link( $comment->comment_ID ) ),
								get_comment_time( 'c' ),
								/* translators: 1: date, 2: time */
								sprintf( __( '%1$s at %2$s', 'didesigns' ), get_comment_date(), get_comment_time() )
							)
						);
					?>

					<?php edit_comment_link( __( 'Edit', 'didesigns' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- .comment-author .vcard -->

				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'didesigns' ); ?></em>
					<br />
				<?php endif; ?>

			</footer>

			<div class="comment-content"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'didesigns' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
endif; // ends check for didesigns_comment()

if ( ! function_exists( 'didesigns_posted_on' ) ) :
/**
 * Print HTML with meta information for the current post-date/time and author.
 *
 * Create your own didesigns_posted_on to override in a child theme
 *
 * @since Di Designs 1.0
 */
function didesigns_posted_on() {
	printf( __( '<span class="sep">Posted on </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a><span class="by-author"> <span class="sep"> by </span> <span class="author vcard"><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></span></span>', 'didesigns' ),
		esc_url( get_permalink() ),
		esc_attr( get_the_time() ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		esc_attr( sprintf( __( 'View all posts by %s', 'didesigns' ), get_the_author() ) ),
		get_the_author()
	);
}
endif;

/**
 * Add two classes to the array of body classes.
 *
 * The first is if the site has only had one author with published posts.
 * The second is if a singular post being displayed
 *
 * @since Di Designs 1.0
 *
 * @param array $classes Existing body classes.
 * @return array The filtered array of body classes.
 */
function didesigns_body_classes( $classes ) {

	if ( function_exists( 'is_multi_author' ) && ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_singular() && ! is_home() && ! is_page_template( 'showcase.php' ) && ! is_page_template( 'sidebar-page.php' ) )
		$classes[] = 'singular';

	return $classes;
}
add_filter( 'body_class', 'didesigns_body_classes' );

/**
 * Retrieve the IDs for images in a gallery.
 *
 * @uses get_post_galleries() First, if available. Falls back to shortcode parsing,
 *                            then as last option uses a get_posts() call.
 *
 * @since Di Designs 1.6
 *
 * @return array List of image IDs from the post gallery.
 */
function didesigns_get_gallery_images() {
	$images = array();

	if ( function_exists( 'get_post_galleries' ) ) {
		$galleries = get_post_galleries( get_the_ID(), false );
		if ( isset( $galleries[0]['ids'] ) )
		 	$images = explode( ',', $galleries[0]['ids'] );
	} else {
		$pattern = get_shortcode_regex();
		preg_match( "/$pattern/s", get_the_content(), $match );
		$atts = shortcode_parse_atts( $match[3] );
		if ( isset( $atts['ids'] ) )
			$images = explode( ',', $atts['ids'] );
	}

	if ( ! $images ) {
		$images = get_posts( array(
			'fields'         => 'ids',
			'numberposts'    => 999,
			'order'          => 'ASC',
			'orderby'        => 'menu_order',
			'post_mime_type' => 'image',
			'post_parent'    => get_the_ID(),
			'post_type'      => 'attachment',
		) );
	}

	return $images;
}

/* ------------------------- CUSTOM FUNCTIONS FROM THIS POINT ON ----------------------------- */
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_register_script('jquerys', "https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js", false, '', true);
   wp_enqueue_script('jquerys');
	 wp_register_script('flex', "https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/jquery.flexslider-min.js", false, null, true);
   wp_enqueue_script('flex');
	 wp_enqueue_script( 'alls', get_stylesheet_directory_uri() . '/js/all.js', array(), '', true );
	 wp_enqueue_script('alls');
	 wp_register_script('awesome', "https://kit.fontawesome.com/593ff87699.js", false, '', true);
	 wp_enqueue_script('awesome');
	 wp_register_script('simplescroll', "https://unpkg.com/simplebar@latest/dist/simplebar.min.js", false, null, true);
	 wp_enqueue_script('simplescroll');
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}

function wordpress_breadcrumbs() {
 
  $delimiter = '/';
  $name = 'Home'; 
  $currentBefore = '<span class="current">';
  $currentAfter = '</span>';
 
  if ( !is_front_page() || is_paged() ) {
  
    global $post;
    $home = get_bloginfo('url');
    echo '<a href="' . $home . '">' . $name . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
      
		// Get the URL of this category
		$category_link = get_category_link( $thisCat );
		echo '<a href="'.get_permalink( get_option( 'page_for_posts' ) ).'">Inspiration</a> / ';
		echo $currentBefore;
	  single_cat_title();
	  echo $currentAfter;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('d') . $currentAfter;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $currentBefore . get_the_time('F') . $currentAfter;
 
    } elseif ( is_year() ) {
      echo $currentBefore . get_the_time('Y') . $currentAfter;	
			
    } elseif(is_home()) {
			echo $currentBefore;
      echo 'Inspiration';
      echo $currentAfter;
			
		} elseif ( is_single() ) {
      $cat = get_the_category(); $cat = $cat[0];
			echo '<a href="'.get_permalink( get_option( 'page_for_posts' ) ).'">Inspiration</a> / ';
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ' . $delimiter . ' ';
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_search() ) {
      echo $currentBefore . 'Search results for &#39;' . get_search_query() . '&#39;' . $currentAfter;
 
    } elseif ( is_tag() ) {
      echo $currentBefore . 'Posts tagged &#39;';
      single_tag_title();
      echo '&#39;' . $currentAfter;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $currentBefore . 'Articles posted by ' . $userdata->display_name . $currentAfter;
 
    } 
		
		elseif(is_tax('yith_product_brand')) {
			echo '<a href="/collections/">Collections</a> ' . $delimiter . ' ';
			echo single_term_title();
		}
		
		elseif(is_tax('product_cat')) {
			echo single_term_title();
		}
		
		elseif(is_shop()) {
			$post_id = get_option( 'woocommerce_shop_page_id' );
			echo $currentBefore . get_the_title($post_id) . $currentAfter;
		}
 		
		elseif ( is_404() ) {
      echo $currentBefore . 'Error 404' . $currentAfter;
    }
 
    /*if ( get_query_var('paged') ) {
			echo ' '.$delimiter.' ';	echo $currentBefore;
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo $currentBefore .__('Page') . ' ' . get_query_var('paged'). $currentAfter;
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
			echo $currentAfter;
    }*/
  
  }
}

function dan_pagination($pages = '', $range = 6)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='paginationer'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}

if ( function_exists( 'YITH_WCTM' ) ) {

	add_filter( 'woocommerce_product_get_price', 'show_product_price', 10, 2 );
	add_filter( 'woocommerce_get_price_html', 'show_product_price', 10, 2 );
	add_filter( 'woocommerce_get_variation_price_html', 'show_product_price', 10, 2 );


	function show_product_price( $price, $product ) {

		if ( defined( 'WOOCOMMERCE_CHECKOUT' ) || defined( 'WOOCOMMERCE_CART' ) || is_admin() ) {
			return $price;
		}

		if ( ! is_user_logged_in() ) {
			$price = get_option( 'ywctm_exclude_price_alternative_text' );
		}

		return $price;

	}
	
}

function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

add_action( 'after_setup_theme', 'yourtheme_setup' );
 
function yourtheme_setup() {
    //add_theme_support( 'wc-product-gallery-zoom' );
    //add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');

function woocommerce_header_add_to_cart_fragment( $fragments ) {
  global $woocommerce;

  ob_start();

  ?>
  <span class="cart-count"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
  <?php

  $fragments['span.cart-count'] = ob_get_clean();

  return $fragments;

}

add_action( 'woocommerce_after_add_to_cart_quantity', 'ts_quantity_plus_sign' );
 
function ts_quantity_plus_sign() {
   echo '<div class="plus" ></div>';
}
 
add_action( 'woocommerce_before_add_to_cart_quantity', 'ts_quantity_minus_sign' );
function ts_quantity_minus_sign() {
   echo '<div type="button" class="minus" ></div>';
}
 
add_action( 'wp_footer', 'ts_quantity_plus_minus' );
 
function ts_quantity_plus_minus() {
   // To run this on the single product page
   if ( ! is_product() ) return;
   ?>
   <script type="text/javascript">
          
      jQuery(document).ready(function($){   
          
            jQuery('form.cart').on( 'click', '.plus, .minus', function() {
 
            // Get current quantity values
            var qty = $( this ).closest( 'form.cart' ).find( '.qty' );
            var val   = parseFloat(qty.val());
            var max = parseFloat(qty.attr( 'max' ));
            var min = parseFloat(qty.attr( 'min' ));
            var step = parseFloat(qty.attr( 'step' ));
 
            // Change the value if plus or minus
            if ( $( this ).is( '.plus' ) ) {
               if ( max && ( max <= val ) ) {
                  qty.val( max );
               } 
            else {
               qty.val( val + step );
                 }
            } 
            else {
               if ( min && ( min >= val ) ) {
                  qty.val( min );
               } 
               else if ( val > 1 ) {
                  qty.val( val - step );
               }
            }
             
         });
          
      });
          
   </script>
   <?php
}

function old_style_name_like_wpse_123298($clauses) {
  remove_filter('term_clauses','old_style_name_like_wpse_123298');
  $pattern = '|(name LIKE )\'{.*?}(.+{.*?})\'|';
  $clauses['where'] = preg_replace($pattern,'$1 \'$2\'',$clauses['where']);
  return $clauses;
}
add_filter('terms_clauses','old_style_name_like_wpse_123298');

class Dan_Walker extends Walker_Nav_Menu {
    // add classes to ul sub-menus
		
	private $curItem;
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        // depth dependent classes
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu',
            'menu-depth-' . $display_depth
        );
        $class_names = implode( ' ', $classes );

        // build html
        if ($display_depth == 1) {
            $output .= "\n" . $indent . '<div class="sub-menu__wrapper">';
						
						$output .= '<div class="inside">' . "\n";
					
						$output .= '<div class="marg flip">' . "\n";
					
						$output .= '<div class="sixty">';
					
						$parentLink = $this->curItem;
					
						$parentClasses = $parentLink->classes;
					
						if (in_array("products-mega", $parentClasses)) {
							
							$output .= '<div class="marg">';
							
							while(has_sub_field('product_menu_block_links', 'option')):
							
							$output .= '<div class="half"><a href="">';
							
							$image = get_sub_field('image');
							$size = 'rect-bigger';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
							
							$link = get_sub_field('link');
							$url = $link['url'];
							$title = $link['title'];
							
							$output .= '<a  class="mega-link"href="'.esc_url($url).'" title="'.esc_attr($title).'"><span class="img-wrap"><img src="'.esc_url($thumb).'" alt="'.esc_attr($alt).'" /></span>';
							
							$output .= '<span class="wrap-link-text"><span>'.get_sub_field('title').'</span>';
							
							$output .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#6ea0af;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg></span>';
							
							$output .= '</a></div>';
							
							endwhile;
							
							$output .= '</div>';
							
						}
					
						elseif (in_array("collections-mega", $parentClasses)) {
							
							$output .= '<div class="marg">';
							
							while(has_sub_field('collection_menu_block_links', 'option')):
							
							$output .= '<div class="half"><a href="">';
							
							$image = get_sub_field('image');
							$size = 'rect-bigger';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
							
							$link = get_sub_field('link');
							$url = $link['url'];
							$title = $link['title'];
							
							$output .= '<a class="mega-link" href="'.esc_url($url).'" title="'.esc_attr($title).'"><span class="img-wrap"><img src="'.esc_url($thumb).'" alt="'.esc_attr($alt).'" /></span>';
							
							$output .= '<span class="wrap-link-text"><span>'.get_sub_field('title').'</span>';
							
							$output .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#6ea0af;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg></span>';
							
							$output .= '</a></div>';
							
							endwhile;
							
							$output .= '</div>';
							
						}
					
						elseif (in_array("about-mega", $parentClasses)) {
							
							$output .= '<div class="marg">';
							
							while(has_sub_field('about_menu_block_links', 'option')):
							
							$output .= '<div class="half"><a href="">';
							
							$image = get_sub_field('image');
							$size = 'rect-bigger';
							$thumb = $image['sizes'][ $size ];
							$width = $image['sizes'][ $size . '-width' ];
							$height = $image['sizes'][ $size . '-height' ];
							
							$link = get_sub_field('link');
							$url = $link['url'];
							$title = $link['title'];
							
							$output .= '<a class="mega-link" href="'.esc_url($url).'" title="'.esc_attr($title).'"><span class="img-wrap"><img src="'.esc_url($thumb).'" alt="'.esc_attr($alt).'" /></span>';
							
							$output .= '<span class="wrap-link-text"><span>'.get_sub_field('title').'</span>';
							
							$output .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#6ea0af;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg></span>';
							
							$output .= '</a></div>';
							
							endwhile;
							
							$output .= '</div>';
							
						}
					
						$output .= '</div>';
					
						$output .= '<div class="fourty menu-wrap">';
					
						if (in_array("about-mega", $parentClasses)) {
					
						$output .= '<div class="extra">';
					
						$output .= get_field('extra_content_about', 'option');
					
						while(has_sub_field('about_extra_links', 'option')):
					
						$link = get_sub_field('link');
					
						$output .= '<a href="'.$link['url'].'"><span>'.$link['title'];
					
						$output .= '</span>'; 
						
						$output .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#6ea0af;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg></span>';
					
						$output .= '</a><br />';
					
						endwhile;
					
						$output .= '</div>';
							
						} elseif (in_array("collections-mega", $parentClasses)) {
					
						$output .= '<div class="extra">';
					
						$output .= get_field('extra_content_collections', 'option');
					
						while(has_sub_field('collections_extra_links', 'option')):
					
						$link = get_sub_field('link');
					
						$output .= '<a href="'.$link['url'].'"><span>'.$link['title'];
					
						$output .= '</span>'; 
						
						$output .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#6ea0af;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg></span>';
					
						$output .= '</a><br />';
					
						endwhile;
					
						$output .= '</div>';
							
						} elseif (in_array("products-mega", $parentClasses)) {
					
						$output .= '<div class="extra">';
					
						$output .= get_field('extra_content_products', 'option');
					
						while(has_sub_field('products_extra_links', 'option')):
					
						$link = get_sub_field('link');
					
						$output .= '<a href="'.$link['url'].'"><span>'.$link['title'];
					
						$output .= '</span>'; 
						
						$output .= '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#6ea0af;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg></span>';
					
						$output .= '</a><br />';
					
						endwhile;
					
						$output .= '</div>';
							
						} 
					
						$output .= '<ul class="' . $class_names . '">' . "\n";
								
					
        } else {
            $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
        }
    }
	
			
	
	function start_el(&$output, $item, $depth=0, $args=[], $id=0) {
		$this->curItem = $item;
		$output .= "<li class='" .  implode(" ", $item->classes) . "'>";
 
		if ($item->url && $item->url != '#') {
			$output .= '<a href="' . $item->url . '">';
		} else {
			$output .= '<span>';
		}
 
		$output .= $item->title;
 
		if ($item->url && $item->url != '#') {
			$output .= '</a>';
		} else {
			$output .= '</span>';
		}
	}
	
}

function create_post_type_labels($singular, $plural = NULL) {

	$plural = $plural === NULL ? $singular . 's' : $plural;

	return array(
		'name'					=> $plural,
		'singular_name'			=> $singular,
		'add_new'				=> 'Add ' . $singular,
		'add_new_item'			=> 'Add New ' . $singular,
		'edit_item'				=> 'Edit ' . $singular,
		'new_item'				=> 'New ' . $singular,
		'view_item'				=> 'View ' . $singular,
		'search_items'			=> 'Search ' . $plural,
		'not_found'				=> 'No ' . strtolower($plural) . ' found',
		'not_found_in_trash'	=> 'No ' . strtolower($plural) . ' found in Trash',
		'parent_item_colon'		=> ''
	);

}

function create_post_type_taxonomy_labels($singular, $plural = NULL) {

	$plural = $plural === NULL ? $singular . 's' : $plural;

	return array(
		'name'              	=> $singular,
		'singular_name'     	=> $plural,
		'search_items'      	=> 'Search ' . $plural,
		'all_items'         	=> 'All ' . $plural,
		'parent_item'       	=> 'Parent ' . $plural,
		'parent_item_colon' 	=> 'Parent ' . $plural,
		'edit_item'         	=> 'Edit ' . $plural,
		'update_item'       	=> 'Update ' . $plural,
		'add_new_item'      	=> 'Add New ' . $plural,
		'new_item_name'     	=> 'New ' . $plural,
		'menu_name'         	=> $plural
	);

}

function register_cpt() {
register_post_type('download', array(
		'labels'				=> create_post_type_labels('Download', 'Download Items'),
		'public'				=> TRUE,
		'query_var'				=> TRUE,
		'capability_type'		=> 'post',
		//'taxonomies'			=> array('news_category'),
		'supports'				=> array('title', 'editor', 'excerpt', 'revisions', 'thumbnail', 'author', 'page-attributes'),
		'has_archive'			=> TRUE,
		'menu_position' 		=> 28,
		'menu_icon'				=> 'dashicons-download'
	));

register_taxonomy( 'download_category', 'download', array(
		'labels' => create_post_type_taxonomy_labels('Download Category', 'Download Categories'),
        'hierarchical' => true,
        'show_admin_column' => true
        //'rewrite'       => array( 'slug' => 'services', 'with_front' => false)
	));
}
add_action( 'init', 'register_cpt', 0 );

add_filter( 'yith_wcan_is_search', '__return_false' );

/*function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Brands Page Sidebar', 'textdomain' ),
        'id'            => 'sidebar-brands',
        'description'   => __( 'Widgets in this area will be shown on brand pages.', 'textdomain' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' ); */

add_filter('woocommerce_sale_flash', 'ds_change_sale_text');
function ds_change_sale_text() {
return '<span class="onsale">Clearance!</span>';
}

add_filter( 'woocommerce_billing_fields', 'ts_require_wc_company_field');
function ts_require_wc_company_field( $fields ) {
$fields['billing_company']['required'] = true;
$fields['shipping_company']['required'] = true;
return $fields;
}

add_filter( 'woocommerce_shipping_fields', 'ts_require_wc_company_fieldd');
function ts_require_wc_company_fieldd( $fields ) {
$fields['shipping_company']['required'] = true;
return $fields;
}

// PRODUCT DIMENSIONS TAB
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
// Adds the new tab
global $post;
$current_post_id = $post->id;
if(get_field('product_dimensions', $current_post_id)) {
$tabs['desc_tab'] = array(
'title' => __( 'Packing Dimensions', 'woocommerce' ),
'priority' => 50,
'callback' => 'woo_new_product_tab_content'

);
}
if(get_field('care_instructions', $current_post_id)) {
$tabs['_tab'] = array(
'title' => __( 'Care Instructions', 'woocommerce' ),
'priority' => 51,
'callback' => 'woo_new_product_tab_content2'

);
}
return $tabs;
}

function woo_new_product_tab_content()  {
    // The new tab content
    $prod_id = get_the_ID();
		the_field('product_dimensions', $prod_id);
}

function woo_new_product_tab_content2()  {
    // The new tab content
    $prod_id = get_the_ID();
		echo '<a href="'.get_field('care_instructions', $prod_id).'" class="button lightgreen">Download care instructions</a>"';
}

/**
 *  CSV download for stock list
 */

function my_account_new_endpoints() {
    add_rewrite_endpoint( 'stocklist', EP_ROOT | EP_PAGES );
}
add_action( 'init', 'my_account_new_endpoints' );

function my_account_menu_order($items) {

    $new = array('stocklist' => __( 'Download Stock List', '' ));

    $reversed = array_reverse($items);

    array_pop($reversed);

    $result = array_merge($reversed, $new);

    $result['dashboard'] = "Dashboard";

    return array_reverse($result);
}
add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );

function stocklist_endpoint_content() {

    // $products = wc_get_products( array(
    //     'limit' => 10,
    // ) );

    // foreach($products as $product){
    //     $title = $product->get_name();
    //     $url = get_permalink( $product->get_id() );
    //     $description = $product->get_description();
    //     $sku = $product->get_sku();
    //     $price = $product->get_price();
    //     $stock_quantity = $product->get_stock_quantity();
    //     $length = $product->get_length();
    //     $width = $product->get_width();
    //     $height = $product->get_height();
    // } 
    ?>

    <h2><?php _e('Download Stock List', 'divi-child'); ?></h2>
    <p><?php _e('Click below to download our up to date stock list as a CSV file.', 'divi-child'); ?></p>
    <p>
        <form method="post" id="download_form" action="">
            <input type="submit" name="download_csv"  class="button button-primary" value="Download Stocklist" />
        </form>
    </p>
<?php 
}
add_action( 'woocommerce_account_stocklist_endpoint', 'stocklist_endpoint_content' );

function func_export_all_posts() {

    if(isset($_POST['download_csv']) && is_account_page()) {

        $products = wc_get_products( array(
            'limit' => -1,
        ) );

        $now = new DateTime();

        $csv_title = 'DI Designs Stock List - ' . $now->format('Y-m-d H:i:s');

        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $csv_title . '.csv"');
        header('Pragma: no-cache');
        header('Expires: 0');

        $file = fopen('php://output', 'w');
  
        fputcsv($file, array('SKU', 'Product', 'Description', 'URL', 'Price', 'Stock Quantity', 'Length', 'Width', 'Height'));

        foreach($products as $product){
            $sku = $product->get_sku();
            $title = $product->get_name();
            $description = $product->get_description();
            $url = get_permalink( $product->get_id() );
            $price = $product->get_price();
            $stock_quantity = $product->get_stock_quantity();
            $length = $product->get_length();
            $width = $product->get_width();
            $height = $product->get_height();

            fputcsv($file, array($sku, $title, $description, $url, $price, $stock_quantity, $length, $width, $height));
        }

        fclose($file);

        exit();
       
    }
}
add_action( 'wp', 'func_export_all_posts' );

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

// Register main datepicker jQuery plugin script
add_action( 'wp_enqueue_scripts', 'enabling_date_picker' );
function enabling_date_picker() {

    // Only on front-end and checkout page
    if( is_admin() || ! is_checkout() ) return;

    // Load the datepicker jQuery-ui plugin script
    wp_enqueue_script( 'jquery-ui-datepicker' );
    wp_register_style( 'jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
    wp_enqueue_style( 'jquery-ui' );  
}

// Call datepicker functionality in your custom text field
add_action('woocommerce_after_order_notes', 'delivery_date_datepicker_field', 10, 1);
function delivery_date_datepicker_field( $checkout ) {

    date_default_timezone_set('America/Los_Angeles');
    $mydateoptions = array('' => __('Select PickupDate', 'woocommerce' ));

    echo '<div id="delivery_date_checkout_field">';

    // YOUR SCRIPT HERE BELOW
    echo '
    <script>
        jQuery(function($){
            $("#datepicker").datepicker({ 
                minDate: 7,
                beforeShowDay: function (date) {
                    var day = date.getDay();
                    return [(day == 1)];
                },
                dateFormat: "dd-mm-yy"
            });
        });
    </script>';

   woocommerce_form_field( 'delivery_date', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'id'            => 'datepicker',
        'required'      => false,
        'label'         => __('Delivery Date - Week Commencing'),
        'placeholder'       => __('Select Week Commencing Date'),
        'options'     =>   $mydateoptions
        ),
    $checkout->get_value( 'cylinder_collect_date' ));

    echo '</div>';
}

/**
 * Update the order meta with custom fields values
 * */
add_action('woocommerce_checkout_update_order_meta', 'delivery_date_checkout_field_update_order_meta');

function delivery_date_checkout_field_update_order_meta($order_id) {

    if (!empty($_POST['delivery_date'])) {
        update_post_meta($order_id, 'analysis1', sanitize_text_field($_POST['delivery_date']));
    } else {
        update_post_meta($order_id, 'analysis1', 'No delivery date specified');
    }
}

add_action( 'woocommerce_admin_order_data_after_shipping_address', 'edit_woocommerce_checkout_page', 10, 1 );

function edit_woocommerce_checkout_page($order){
    global $post_id;
    $order = new WC_Order( $post_id );
    $order_date = 'No delivery date specified';

    if (get_post_meta($order->get_id(), 'delivery_date', true )){
        $order_date = get_post_meta($order->get_id(), 'delivery_date', true );
    }

    if(get_post_meta($order->get_id(), 'analysis1', true )){
        $order_date = get_post_meta($order->get_id(), 'analysis1', true );
    }

    echo '<p><strong class="delivery_date">'.__('Delivery Date').':</strong> ' . $order_date . '</p>';
}

/**
 *  Add button link to dashboard to approve user
 */

add_action('wp_dashboard_setup', 'kwb_link_custom_dashboard_widgets');
  
function kwb_link_custom_dashboard_widgets() {
    global $wp_meta_boxes;
    
    wp_add_dashboard_widget('custom_help_widget', 'New Trade Membership', 'kwb_link_custom_dashboard_help', null, null, 'normal', 'high');
}
 
function kwb_link_custom_dashboard_help() {
    echo '<p><a style="border: green; background:green; color: #fff;" class="button" href="' . get_admin_url() . 'admin.php?page=gf_entries&id=2">Activate new trade memberships</a></p>';
}