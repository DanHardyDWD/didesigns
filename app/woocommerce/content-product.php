<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( '', $product ); ?>>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );
	?>
	
	<div class="row product-meta">
	
		<?php $productID = $product->id; ?>
		
		<div class="skus">
			<?php echo $product->get_sku(); ?>
		</div>
		
		<div class="prod-col">
			<?php the_terms($productID, 'yith_product_brand'); ?>
		</div>
		
	</div>
	
	<?php 
	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	if(is_user_logged_in()) {
		woocommerce_template_loop_price();
	} else {
		echo '<div class="price">Login to view price</div>';
	}
		
 
	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	?>
	
	<?php echo wc_get_stock_html($product); ?>
	
	<?php if(is_user_logged_in())	{ ?>
		<?php woocommerce_template_loop_add_to_cart(); ?>
	<?php } else { ?>
		<div class="wrap-sign-up-button">
		<a href="/my-account/" class="button lightgreen more-marg no-rad">Login or register</a>
		</div>
	<?php } ?>
	
	<?php //do_action( 'woocommerce_after_shop_loop_item' );
	?>
</li>
