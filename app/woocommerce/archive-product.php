<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( ); ?>

<?php if(!is_tax('yith_product_brand')) { ?>

<?php $post_id = get_option( 'woocommerce_shop_page_id' ); if(has_post_thumbnail($post_id)) { ?>
<div class="hero-and-crumbs row">

	<div class="hero-top row">

		<img src="<?php echo get_the_post_thumbnail_url($post_id,'large'); ?> ?>" class="abs-image-insp" alt="<?php the_title($post_id); ?>">
		
		<div class="inside">
		
			<?php $title = get_field('custom_page_title', $post_id); if($title) { $pageTitle = $title; } else { $pageTitle = get_the_title($post_id); } ?>
			
			<h1><?php echo $pageTitle; ?></h1>
			
		</div>
		
	</div>
	
	<div class="crumbs row">
	
		<div class="inside">
			<?php wordpress_breadcrumbs(); ?>
		</div>
		
	</div>
	
</div>
<?php } ?>

<?php } ?>

<?php if(is_tax('yith_product_brand')) { ?>
			
<?php $term = get_queried_object(); ?>

<div class="row hero brand-hero">

	<div class="inside">
	
		<div class="marg">
		
			<div class="fourty">
			
				<h1><?php echo $term->name; ?></h1>
				
				<?php the_field('introduction_hero', $term); ?>
				
			</div>
			
		</div>
		
	</div>
	
	<img src="<?php the_field('hero_image', $term); ?>" alt="<?php echo $term->name; ?>" class="hero-image">
	
</div>

<div class="crumbs row">
	
	<div class="inside">
		<?php wordpress_breadcrumbs(); ?>
	</div>
		
</div>

<?php $image = get_field('image', $term); if($image) { ?>
<div class="row page-content large-img-w-content">
	
	<div class="inside">
	
		<div class="marg">
		
			<div class="seventy">
			</div>
			
			<div class="thirty">
				
				<?php the_field('introduction', $term); ?>
				
				<div class="row button-wrap">
					<a href="" class="down-to-col button blue">
						Shop the collection
					</a>
				</div>
				
			</div>
			
		</div>
		
	</div>
	
	<?php
			$image = get_field('image', $term);
			if( $image ):
			$url = $image['url'];
			$title = $image['title'];
			$alt = $image['alt'];
			$caption = $image['caption'];
			$size = 'full';
			$thumb = $image['sizes'][ $size ];
			$width = $image['sizes'][ $size . '-width' ];
			$height = $image['sizes'][ $size . '-height' ];

		?>

		<img src="<?php echo esc_url($url ); ?>" alt="<?php echo esc_attr($alt); ?>" />

		<?php endif; ?>
	
</div>
<?php } ?>

<?php $image = get_field('image_m_i', $term); if($image) { ?>
<div class="row more-inf">

	<div class="inside">
		
		<div class="marg">
		
			<div class="half">
			</div>
			
			<div class="half">
				
				<?php the_field('more_information', $term); ?>
				
			</div>
			
		</div>
		
	</div>
	
	<img src="<?php echo $image; ?>" class="abs-image-insp-alt">
	
</div>
<?php } ?>
				
<?php } ?>

<div class="row page-content stnd-content">

	<div class="inside">
    
    	<div class="row main-shop">

				<?php
				/**
				 * Hook: woocommerce_before_main_content.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 * @hooked WC_Structured_Data::generate_website_data() - 30
				 */
				woocommerce_output_content_wrapper();

				?>
				
				<div class="row woo-sort" id="col-area">
					<?php do_action( 'woocommerce_before_shop_loop' ); ?>
				</div>
				
				<div class="row">
						
						<div class="woo-side">
							<?php if(!is_search()) { ?>
							<?php do_action( 'woocommerce_sidebar' ); ?>		
							<?php } else { ?>
							
							<h3>Search results for '<span class='l-green'><?php echo get_search_query(); ?></span>'</h3>
							
							<aside id="yith-woo-ajax-reset-navigation-3" class="widget yith-woocommerce-ajax-product-filter yith-woo-ajax-reset-navigation yith-woo-ajax-navigation woocommerce widget_layered_nav" style=""><div class="yith-wcan"><a class="yith-wcan-reset-navigation button" href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">All products</a></div></aside>
							
							<?php } ?>
						</div>
						
						<div class="woo-main">
				
							<div class="row">
							
							<?php
							if ( woocommerce_product_loop() ) {

								/**
								 * Hook: woocommerce_before_shop_loop.
								 *
								 * @hooked woocommerce_output_all_notices - 10
								 * @hooked woocommerce_result_count - 20
								 * @hooked woocommerce_catalog_ordering - 30
								 */

								woocommerce_product_loop_start();

								if ( wc_get_loop_prop( 'total' ) ) {
									while ( have_posts() ) {
										the_post();

										/**
										 * Hook: woocommerce_shop_loop.
										 */
										do_action( 'woocommerce_shop_loop' );

										wc_get_template_part( 'content', 'product' );
									}
								}

								woocommerce_product_loop_end();

								/**
								 * Hook: woocommerce_after_shop_loop.
								 *
								 * @hooked woocommerce_pagination - 10
								 */
								
								 do_action( 'woocommerce_after_shop_loop' ); ?>
								
								<?php /*
								<div class="pager">
								<?php the_posts_pagination(); ?>
								</div>
								
								<div class="row center">
								
									<div class="page-load-status">
										<div class="loader-ellips infinite-scroll-request">
											
										</div>
									</div>
									
									<div class="view-more-button button lightgreen">
										Load More
									</div>
									
								</div>	<?php */ ?>
									
							<?php } else {
								/**
								 * Hook: woocommerce_no_products_found.
								 *
								 * @hooked wc_no_products_found - 10
								 */
								do_action( 'woocommerce_no_products_found' );
							}

							/**
							 * Hook: woocommerce_after_main_content.
							 *
							 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
							 */
							do_action( 'woocommerce_after_main_content' );

							/**
							 * Hook: woocommerce_sidebar.
							 *
							 * @hooked woocommerce_get_sidebar - 10
							 */
							?>
							</div>
							
						</div>
						
					</div>
				
				</div>

		</div>
		
	</div>

<?php if(is_tax('yith_product_brand')) { ?>

<?php $term = get_queried_object(); $featImg = get_field('background_image_features', $term); if($featImg) { ?>

<div class="row features-collection" style="background-image: url(<?php echo $featImg ?>);">

	<div class="inside">
		
		<div class="marg">
			
			<div class="sixty">
			</div>
			
			<div class="fourty">
			
				<?php while(has_sub_field('features', $term)): ?>
				
				<div class="row feature">
				
					<?php the_sub_field('feature'); ?>
					
				</div>
				
				<?php endwhile; ?>
				
			</div>
			
		</div>
		
	</div>
	
</div>

<?php } ?>

<?php $gallery = get_field('gallery', $term); if($gallery) { ?>

<div class="row page-content stnd-content">

	<div class="inside">
		
		<div class="wrap-col-gallery">
		
			<div class="row">

				<div class="row collection-gallery">

					<?php 
					$images = get_field('gallery', $term);
					if( $images ): ?>
						<?php foreach( $images as $image ): ?>		
						<div class="gallery-slide">	
							<img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
						</div>
						<?php endforeach; ?>
					<?php endif; ?>

				</div>

			</div>
			
		</div>
		
	</div>
	
</div>

<?php } ?>

<?php }?>
	
<?php get_footer( 'shop' ); ?>
