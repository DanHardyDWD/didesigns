<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<div class="row page-content stnd-content">

	<div class="inside">
    
    	<div class="row main-shop singular">

			<?php
				/**
				 * woocommerce_before_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				woocommerce_output_content_wrapper();
			?>
				
			<div class="wrap-crumbs row">
			
				<?php woocommerce_breadcrumb(); ?>
				
				<div class="woo-nav">
				
					<span>Previous / Next</span>
					
					<?php
					
					$previous = next_post_link('%link', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M17.961,8.588,4.315,8.609l3.891-3.9a.986.986,0,0,0-1.4-1.393L1.237,8.9a.893.893,0,0,0-.065.073.381.381,0,0,0-.024.032c-.012.015-.023.03-.034.046s-.016.026-.024.04-.017.028-.025.042a.4.4,0,0,0-.02.042c-.008.016-.015.03-.022.046l-.014.041C1,9.281,1,9.3.991,9.314s-.007.027-.01.04a.529.529,0,0,0-.013.054c0,.014-.005.029-.007.043S.955,9.486.954,9.5s0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s.005.033.007.05,0,.03.007.045.009.035.013.052.007.028.011.042.011.033.017.049.01.028.016.042.013.029.02.044.013.029.021.044l.024.04c.009.014.017.029.026.042s.021.029.031.043.017.024.026.035.043.049.066.072l5.587,5.572a.986.986,0,0,0,1.393-1.4l-3.9-3.89,13.646-.021a.987.987,0,1,0,0-1.973Z"/></svg>', TRUE, ' ', 'product_cat');
					$next = previous_post_link('%link', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><defs><style>.a{fill:#fff;}</style></defs><path class="a" d="M1.937,8.588l13.646.021-3.891-3.9a.986.986,0,0,1,1.4-1.393L18.661,8.9a.893.893,0,0,1,.065.073.381.381,0,0,1,.024.032c.012.015.023.03.034.046s.016.026.024.04.017.028.025.042a.4.4,0,0,1,.02.042.469.469,0,0,1,.021.046c.006.014.01.027.015.041s.013.033.018.05.007.027.01.04a.529.529,0,0,1,.013.054c0,.014,0,.029.007.043s.006.035.007.053,0,.043,0,.065,0,.021,0,.031l0,.03c0,.022,0,.044,0,.067s0,.033-.007.05,0,.03-.007.045-.009.035-.013.052-.007.028-.011.042-.011.033-.017.049-.01.028-.016.042-.013.029-.02.044-.013.029-.021.044l-.024.04c-.009.014-.017.029-.026.042s-.021.029-.031.043-.017.024-.026.035-.043.049-.066.072l-5.587,5.572a.986.986,0,0,1-1.393-1.4l3.9-3.89L1.934,10.561a.987.987,0,0,1,0-1.973Z"/></svg>', TRUE, ' ', 'product_cat');

					echo $previous;
					echo $next;				
	
					?>
					
				</div>
				
			</div>

		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
	?>

			</div>

		</div>
		
	</div>				
				
<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
