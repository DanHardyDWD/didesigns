<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	?>

	<div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images">
			
					<div class="custom-slider">
					
						<div class="row custom-image-slider">
						
							<div class="custom-image-slide woocommerce-product-gallery__image">
								<?php $post_thumbnail_id = $product->get_image_id(); ?>
								<a data-lightbox-gallery="gallery-<?php the_ID(); ?>" class="image-zoom first" href="<?php echo wp_get_attachment_image_url( $post_thumbnail_id, 'full'); ?>" rel="lightbox">
									<img src="<?php echo wp_get_attachment_image_url( $post_thumbnail_id, 'large'); ?>" alt="Two drawer desk" class="flex-active" draggable="false" data-zoom="<?php echo wp_get_attachment_image_url( $post_thumbnail_id, 'full'); ?>">
								</a>
							</div>

							<?php $attachment_ids = $product->get_gallery_image_ids(); ?>
							<?php foreach ( $attachment_ids as $attachment_id ) { ?>
							<div class="custom-image-slide woocommerce-product-gallery__image">
								<a data-lightbox-gallery="gallery-<?php the_ID(); ?>" class="image-zoom" href="<?php echo wp_get_attachment_image_url( $attachment_id, 'full'); ?>" rel="lightbox">
									<img src="<?php echo wp_get_attachment_image_url( $attachment_id, 'large'); ?>" alt="Two drawer desk" class="flex-active" draggable="false" data-zoom="<?php echo wp_get_attachment_image_url( $attachment_id, 'full'); ?>">
								</a>
							</div>
							<?php } ?>
							
						</div>
						
					</div>
					
					<ol class="flex-control-nav flex-control-thumbs" style="width: auto; position: relative; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">

						<li class="custom-open-slide-img active" data-open="0">
							<img src="<?php echo wp_get_attachment_image_url( $post_thumbnail_id, 'thumbnail'); ?>" alt="<?php the_title(); ?>" class="flex-active" draggable="false">
						</li>
						
						<?php $num = 1;

						if ( $attachment_ids && $product->get_image_id() ) {
							foreach ( $attachment_ids as $attachment_id ) { ?>

						<li class="custom-open-slide-img" data-open="<?php echo $num; ?>">
							<img src="<?php echo wp_get_attachment_image_url( $attachment_id, 'thumbnail'); ?>" alt="<?php the_title(); ?>" class="flex-active" draggable="false">
						</li>

						<?php $num++; } } ?>

					</ol>
					
				</div>
			
				<div class="row wrap-mobile-thumbs">
			
					<ol class="mobile-thumbs" style="width: auto; position: relative; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">

							<li class="custom-open-slide-img active thumb" data-open="0">
								<img src="<?php echo wp_get_attachment_image_url( $post_thumbnail_id, 'thumbnail'); ?>" alt="<?php the_title(); ?>" class="flex-active" draggable="false">
							</li>

							<?php $num = 1;

							if ( $attachment_ids && $product->get_image_id() ) {
								foreach ( $attachment_ids as $attachment_id ) { ?>

							<li class="custom-open-slide-img thumb" data-open="<?php echo $num; ?>">
								<img src="<?php echo wp_get_attachment_image_url( $attachment_id, 'thumbnail'); ?>" alt="<?php the_title(); ?>" class="flex-active" draggable="false">
							</li>

							<?php $num++; } } ?>

						</ol>
			
					</div>
	
	<div class="summary entry-summary">
		<div class="wrap-summary row">
		<?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
	
		woocommerce_template_single_title();
		woocommerce_template_single_rating();
		?>
			
		<div class="row short-desc">	
			
		<?php echo wp_trim_words(get_the_content(), 20); ?>
			
		<br /><a href="" class="view-full-desc inner">
			Full description	
		</a>	
			
		</div>

		<?php if(is_user_logged_in()) {
			woocommerce_template_single_price();
		} else {
			echo '<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">Register/Sign in as a trade member to see prices</bdi></span></p>';
		} ?>	
			
		<div class="row product-meta">
	
			<?php $productID = $product->id; ?>

			<div class="skus">
				<?php echo $product->get_sku(); ?>
			</div>

			<div class="prod-col">
				<?php the_terms($productID, 'yith_product_brand'); ?>
			</div>

		</div>	
	
		<div class="row prod-tags">
			Tags: <?php the_terms($productID,'product_cat'); ?>	
		</div>	
			
		<?php $notes = get_field('notes'); if($notes) { ?>
		<div class="row product-notes">
			<?php echo $notes; ?>
		</div>
		<?php } ?>	
			
		<?php //echo do_shortcode('[addtoany]'); ?>	
			
		<?php if(is_user_logged_in())	{ ?>
		<?php woocommerce_template_single_add_to_cart(); ?>
		<?php } else { ?>
		<div class="wrap-sign-up-button">
		<a href="/my-account/" class="button lightgreen more-marg no-rad">Login or register</a>
		</div>
		<?php } ?>	
		
		</div>
	</div>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	//do_action( 'woocommerce_after_single_product_summary' );
	?>
	
</div>

<?php $banner = get_field('banner_image'); if($banner) { ?>
<!-- close main row -->
</div></div></div>
<div class="banner row">
	<img src="<?php echo $banner; ?>" alt="<?php the_title(); ?>">
</div>
<!-- reopen main row -->
<div class="row page-content no-bottom-p stnd-content">

	<div class="inside">
    
    	<div class="row main-shop">
<?php } ?>

<div class="row tabs-area">
	
	<?php
	woocommerce_output_product_data_tabs();
	
	?>
	
</div>
				
<?php //woocommerce_upsell_display(); ?>

<!-- close main row -->
</div></div></div>
<!-- reopen main row -->
<div class="row page-content stnd-content">

	<div class="inside">
    
    	<div class="row main-shop woo-main no-p">
				
				<?php woocommerce_output_related_products(); ?>

<?php do_action( 'woocommerce_after_single_product' ); ?>

				</div></div></div>
<div class="row wrap-insp">
<?php include(locate_template('partials/inspiration_corner.php')); ?>
<div></div>
	
