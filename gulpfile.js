var gulp = require("gulp"),
    sass = require("gulp-sass"),
    postcss = require("gulp-postcss"),
    autoprefixer = require("autoprefixer"),
    cssnano = require("cssnano"),
    sourcemaps = require("gulp-sourcemaps"),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),   
	minify = require('gulp-minify'),
	cleanCSS = require('gulp-clean-css'),
  changed = require('gulp-changed'),
	prefix = require('gulp-autoprefixer'),
	postcss  = require('gulp-postcss'),
  headerComment = require('gulp-header-comment'),
	imagemin = require('gulp-imagemin');
	// Uncomment this line to require tinyjpg - API Key is needed
	//require('gulp-tiny');	

var paths = {
    styles: {
        src: "app/scss/",
        dest: "Dist"
    },
	scripts: {
        src: "app/js/*.js",
        dest: "Dist/js"
	},
	images: {
        src: "app/images/*",
        dest: "Dist/images"
	},
	templates: {
        src:(['app/**/*',
      '!app/{images,js,scss,css}/**/*',
	  '!app/scss/']),
        dest: "Dist/"
    }
};

const styleHead = "Theme Name: Di Designs \n Theme URI: http://didesigns.co.uk \n Author: Mooch Creative \n Author URI: http://moochcreative.co.uk \n Description: A custom made theme for Di Designs. \n Version: 1.0 \n License: GNU General Public License v2 or later \n License URI: http://www.gnu.org/licenses/gpl-2.0.html \n Tags: dark, light, white, black, gray, one-column, two-columns, left-sidebar, right-sidebar, fixed-layout, responsive-layout, custom-background, custom-colors, custom-header, custom-menu, editor-style, featured-image-header, featured-images, flexible-header, full-width-template, microformats, post-formats, rtl-language-support, sticky-post, theme-options, translation-ready \n Text Domain: tec2019";

function css() {
    return gulp
        .src(paths.styles.src+'styles.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
		.pipe(concat('style.css'))
		.pipe(postcss([ autoprefixer() ]))
		.pipe(cleanCSS())
    .pipe(headerComment(styleHead))
		.pipe(gulp.dest('./dist/'))
        .on("error", sass.logError);
}

function themefiles() {
    return gulp
        .src(paths.templates.src)
		.pipe(gulp.dest('./dist/'))
}

function script() {
    return gulp
        .src(paths.scripts.src)
        .pipe(concat('all.js'))
		.pipe(minify({
    ext:{
        min:'.js'
    },
    noSource: true
}))
		.pipe(gulp.dest('./dist/js/'))
}

function image() {
    return gulp
        .src(paths.images.src)
        .pipe(changed(paths.images.dest))
        .pipe(imagemin({
		optimizationLevel: 3,
      	progressive: true,
      	interlaced: true}))
        .pipe(gulp.dest('./dist/images/'));
}

function watch() {
  gulp.watch(paths.styles.src, css);
	gulp.watch(paths.scripts.src, script);
	gulp.watch(paths.templates.src, themefiles);
	gulp.watch(paths.images.src, image);
}
    
exports.watch = watch

exports.style = css;
exports.script = script;
exports.image = image;
exports.themefiles = themefiles;

var build = gulp.parallel(css, watch, script, themefiles, image);

gulp.task('default', build);